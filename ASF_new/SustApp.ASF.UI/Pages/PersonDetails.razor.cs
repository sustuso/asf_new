﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SustApp.ASF.DomainModel;
using SustApp.ASF.UI.Forms;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;

namespace SustApp.ASF.UI.Pages
{
    public partial class PersonDetails
    {
        private Person model;


        [Parameter]
        public Guid? Id { get; set; }

        [CascadingParameter]
        public SpinnerContext SpinnerContext { get; set; }

        protected override async Task OnParametersSetAsync()
        {
            await base.OnParametersSetAsync();

            // Avoid loading twice
            // Can happen when navigation go to another page (due tue SpinnerValue)
            if (model != null) return;
            
            if (Id.HasValue)
            {
                await ErrorService.RunAsync(
                    async t =>
                    {
                        model = await PersonServiceAgent.GetAsync(Id.Value, t);
                    }, SpinnerContext, Token);
            }
            else
            {
                model = new Person();
            }
        }

        private Task Save() =>
            ErrorService.RunAsync(
                async t =>
                {
                    if (Id.HasValue)
                    {
                        model.IdASFCompany = Guid.NewGuid();
                        model.IdCompany = Guid.NewGuid();
                        await PersonServiceAgent.UpdateAsync(model, t);
                    }
                    else
                    {
                        await PersonServiceAgent.AddAsync(model, t);
                    }
                    NavigationManager.NavigateTo("/persons");
                }, SpinnerContext, Token);
    }
}
