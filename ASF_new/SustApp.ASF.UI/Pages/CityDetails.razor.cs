﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SustApp.ASF.DomainModel;
using SustApp.ASF.UI.Forms;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;

namespace SustApp.ASF.UI.Pages
{
    public partial class CityDetails
    {
        private City model;

        [Parameter]
        public string Id { get; set; }

        [CascadingParameter]
        public SpinnerContext SpinnerContext { get; set; }

        protected override async Task OnParametersSetAsync()
        {
            await base.OnParametersSetAsync();

            // Avoid loading twice
            // Can happen when navigation go to another page (due tue SpinnerValue)
            if (model != null) return;

            if (!string.IsNullOrEmpty(Id))
            {
                await ErrorService.RunAsync(
                    async t =>
                    {
                        model = await CityServiceAgent.GetAsync(Id, t);
                    }, SpinnerContext, Token);
            }
            else
            {
                model = new City();
            }
        }
        private Task Save() =>
            ErrorService.RunAsync(
                async t =>
                {
                    if (!String.IsNullOrEmpty(Id))
                    {
                        await CityServiceAgent.UpdateAsync(model, t);
                    }
                    else
                    {
                        await CityServiceAgent.AddAsync(model, t);
                    }
                    NavigationManager.NavigateTo("/cities");
                }, SpinnerContext, Token);

    
    }
}
