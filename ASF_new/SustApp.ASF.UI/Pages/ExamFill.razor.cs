﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SustApp.ASF.DomainModel;
using SustApp.ASF.UI.Forms;
using SustApp.ASF.UI.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;

namespace SustApp.ASF.UI.Pages
{
    public partial class ExamFill
    {
        private readonly List<FormFieldResult> formFieldResults = new List<FormFieldResult>();

        [Parameter]
        public Guid Id { get; set; }

        [CascadingParameter]
        public SpinnerContext SpinnerContext { get; set; }

        private EditContext editContext;

        private Guid? examResultId;

        protected override void OnInitialized()
        {
            base.OnInitialized();

            editContext = new EditContext(this);
        }

        protected override async Task OnParametersSetAsync()
        {
            await base.OnParametersSetAsync();

            editContext.AddFormFieldValidation(formFieldResults);

            await ErrorService.RunAsync(async t =>
            {
                CountResult<FormFieldResult> formFieldResult = null;

                // Look for
                var examResultRequest = new GetRequest<ExamResult>();
                examResultRequest.QueryFactory = q => q.Where(f => f.ExamId == Id);

                var examResult = await ExamResultServiceAgent.GetAsync(examResultRequest, t);
                if (examResult.Items.Count > 0)
                {
                    var formFieldResultRequest = new GetRequest<FormFieldResult>();
                    examResultId = examResult.Items[0].Id;
                    formFieldResultRequest.QueryFactory = q => q.Where(f => f.ExamResultId == examResultId);

                    formFieldResult = await FormFieldResultServiceAgent.GetAsync(formFieldResultRequest, t);
                }

                var request = new GetRequest<FormField>();
                request.QueryFactory = q => q.Where(f => f.ExamId == Id);

                var formFields = await FormFieldServiceAgent.GetAsync(request, t);

                foreach (FormField formField in formFields.Items)
                {
                    FormFieldResult result = null;
                    if (formFieldResult != null)
                    {
                        result = formFieldResult.Items.FirstOrDefault(f => f.FormFieldId == formField.Id);
                    }

                    result ??= new FormFieldResult();

                    result.FormField = formField;
                    result.FormFieldId = formField.Id;

                    formFieldResults.Add(result);
                }

            }, SpinnerContext, Token);
        }

        private Task Save() => ErrorService.RunAsync(async t =>
        {
            if (!examResultId.HasValue)
            {
                ExamResult examResult = await ExamResultServiceAgent.AddAsync(new ExamResult
                {
                    Date = DateTimeOffset.Now,
                    ExamId = Id
                }, t);
                examResultId = examResult.Id;
            }

            foreach (FormFieldResult formFieldResult in formFieldResults)
            {
                formFieldResult.ExamResultId = examResultId.Value;
                formFieldResult.FormField = null;

                if (formFieldResult.Id == default)
                {
                    await FormFieldResultServiceAgent.AddAsync(formFieldResult, t);
                }
                else
                {
                    await FormFieldResultServiceAgent.UpdateAsync(formFieldResult, t);
                }
            }
        }, SpinnerContext, Token);
    }
}
