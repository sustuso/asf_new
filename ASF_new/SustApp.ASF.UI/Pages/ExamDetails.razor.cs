﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SustApp.ASF.DomainModel;
using SustApp.ASF.UI.Forms;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;

namespace SustApp.ASF.UI.Pages
{
    public partial class ExamDetails
    {
        private Exam model;

        private Measurement measurement;

        private RowAutoInputSelect<Measurement, string> measurementSelect;

        [Parameter]
        public Guid? Id { get; set; }

        [CascadingParameter]
        public SpinnerContext SpinnerContext { get; set; }

        protected override async Task OnParametersSetAsync()
        {
            await base.OnParametersSetAsync();

            // Avoid loading twice
            // Can happen when navigation go to another page (due tue SpinnerValue)
            if (model != null) return;
            
            if (Id.HasValue)
            {
                await ErrorService.RunAsync(
                    async t =>
                    {
                        model = await ExamServiceAgent.GetAsync(Id.Value, t);
                    }, SpinnerContext, Token);
            }
            else
            {
                model = new Exam();
            }
        }

        private void OnLoadMeasurements(RowAutoInputSelectEventArgs<Measurement, String> args)
        {
            // Esempio di filtro aggiuntivo
            args.Query = args.Query.Where(e => e.Id.Length >= model.MedicalSpecializationId.Length).Take(100);
        }

        private async Task OnMedicalSpecializationChanged(string value)
        {
            model.MedicalSpecializationId = value;

            await measurementSelect.LoadItemsAsync();
        }

        private Task Save() =>
            ErrorService.RunAsync(
                async t =>
                {
                    if (Id.HasValue)
                    {
                        await ExamServiceAgent.UpdateAsync(model, t);
                    }
                    else
                    {
                        await ExamServiceAgent.AddAsync(model, t);
                    }
                    NavigationManager.NavigateTo("/exams");
                }, SpinnerContext, Token);
    }
}
