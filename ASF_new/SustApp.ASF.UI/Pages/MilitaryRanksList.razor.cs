﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Blazorise.DataGrid;
using SustApp.ASF.DomainModel;
using SustApp.ASF.UI.Forms;
using SustApp.ASF.UI.ServiceAgent;
using SustApp.ASF.UI.Services;
using Microsoft.AspNetCore.Components;

namespace SustApp.ASF.UI.Pages
{
    public partial class MilitaryRanksList
    {

        [CascadingParameter]
        public SpinnerContext SpinnerContext { get; set; }

        private CountResult<MilitaryRank> countResult = CountResult<MilitaryRank>.Empty;

        private DataGridReadDataEventArgs<MilitaryRank> lastDataGrid;

        private string _searchText;

        private CancellationTokenSource _refreshTokenSource;

        private string searchText
        {
            get => _searchText;
            set
            {
                _searchText = value;
                _ = Refresh(true);
            }
        }

        private async Task OnRowRemoved(MilitaryRank e)
        {
            if (await DialogService.ConfirmAsync("Conferma", $"Vuoi davvero rimuovere '{e.Name}'?"))
            {
                await ErrorService.RunAsync(t => MilitaryRankServiceAgent.DeleteAsync(e, t), Token);
                await Refresh(false);
            }
        }

        private void OnEdit(MilitaryRank e)
        {
            NavigationManager.NavigateTo($"/militaryranks/{e.Id}");
        }

        private Task OnReadData(DataGridReadDataEventArgs<MilitaryRank> e)
        {
            lastDataGrid = e;
            return Refresh(false);
        }

        private Task Refresh(bool wait)
        {
            _refreshTokenSource?.Cancel();

            _refreshTokenSource = new CancellationTokenSource();

            // Handle component Token and refresh token
            var combinedToken = CancellationTokenSource.CreateLinkedTokenSource(Token, _refreshTokenSource.Token).Token;
            return Refresh(wait, combinedToken);
        }

        private async Task Refresh(bool wait, CancellationToken token)
        {
            // Wait if needed
            if (wait)
            {
                await Task.Delay(200);
                if (token.IsCancellationRequested) return;
            }

            await ErrorService.RunAsync(async t =>
            {
                var request = new GetRequest<MilitaryRank, MilitaryRank>
                {
                    WithCount = true,
                    QueryFactory = e => e.WhereAnyProperty(searchText).AppyDataGrid(lastDataGrid),
                    //ExpandExpressions =
                    //{
                    //    e => e.Measurement
                    //}
                };

                countResult = await MilitaryRankServiceAgent.GetAsync(request, t);

                // Reload grid items
                StateHasChanged();
            }, SpinnerContext, token);
        }
    }
}
