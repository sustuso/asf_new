﻿
using Blazorise;
using Blazorise.Bootstrap;
using Blazorise.Icons.FontAwesome;
using SustApp.ASF.UI.Forms;
using SustApp.ASF.UI.Forms.Services;
#if !DESKTOP
using System.Collections.Generic;
using System.Threading.Tasks;
using Blazored.Modal;
using Blazored.Toast;
using SustApp.ASF.UI.Contexts;
using SustApp.ASF.UI.Services;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace SustApp.ASF.UI
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("app");

            builder.Services.AddBaseAddressHttpClient();

            builder.Services.AddOidcAuthentication(options =>
            {
                // TODO: impostare indirizzo di produzione
                options.ProviderOptions.Authority = "https://localhost:44383";
                options.ProviderOptions.ClientId = "ASF.UI";
                options.ProviderOptions.DefaultScopes = new List<string> { "ASF.Api", "openid", "profile" };
                options.ProviderOptions.ResponseType = "code";
                options.UserOptions.RoleClaim = "role";
                options.UserOptions.NameClaim = "sub";
            });

            ConfigureServices(builder.Services);

            await builder.Build().RunAsync();
        }

        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<UserContext>();

            // Registro come scoped (singola istanza lato server e lato client)
            services.AddScoped<ErrorService>();
            // Anche per l'interfaccia uso la stessa istanza registrata nella riga precedente
            services.AddTransient<IErrorService>(p => p.GetRequiredService<ErrorService>());

            services.AddServiceAgent();

            services.AddASFForms();

            services.AddBlazorise()
                .AddBootstrapProviders()
                .AddFontAwesomeIcons();
        }
    }
}
#endif