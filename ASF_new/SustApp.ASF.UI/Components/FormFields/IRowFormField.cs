﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SustApp.ASF.DomainModel;

namespace SustApp.ASF.UI.Components.FormFields
{

    public class RowFormFieldExpression
    {
        public FormFieldResult Result { get; }

        private RowFormFieldExpression(FormFieldResult result)
        {
            Result = result;
        }

        private string Text;

        private bool Flag;

        public static Expression<Func<string>> GetText(FormFieldResult result)
        {
            var instance = new RowFormFieldExpression(result);
            return () => instance.Text;
        }

        public static Expression<Func<bool>> GetFlag(FormFieldResult result)
        {
            var instance = new RowFormFieldExpression(result);
            return () => instance.Flag;
        }
    }
}
