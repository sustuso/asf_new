﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using SustApp.ASF.UI.Forms;
using SustApp.ASF.UI.Forms.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace SustApp.ASF.UI.Services
{
    public class ErrorService : IErrorService
    {
        private readonly IDialogService _dialogService;
        private readonly ILogger<ErrorService> _logger;
        private readonly NavigationManager _navigationManger;
        private readonly IOptions<RemoteAuthenticationOptions<OidcProviderOptions>> _authenticationOptions;

        //Call from "@inject IErrorService ErrorService" in Index.razor
        public ErrorService(IDialogService dialogService, ILogger<ErrorService> logger, NavigationManager navigationManger, IOptions<RemoteAuthenticationOptions<OidcProviderOptions>> authenticationOptions)
        {
            _dialogService = dialogService;
            _logger = logger;
            _navigationManger = navigationManger;
            _authenticationOptions = authenticationOptions;
        }

        public async Task RunAsync(Func<CancellationToken, Task> task, SpinnerContext spinnerContext, CancellationToken token)
        {
            try
            {
                try
                {
                    //Spinner start:
                    spinnerContext?.IncrementActive();

                    //Block and wait end task on delegate call:
                    await task(token);
                    token.ThrowIfCancellationRequested();
                }
                finally
                {
                    //Spinner end:
                    spinnerContext?.DecrementActive();
                }
            }
            catch (OperationCanceledException)
            {

            }
#if !SERVER
            catch (HttpRequestException ex)
            {
                _logger.LogError("HTTP error occurred", ex);

                _navigationManger.NavigateTo(_authenticationOptions.Value.AuthenticationPaths.LogInPath);
            }
#endif
            catch (Exception ex)
            {
                _logger.LogError("Error while executing task", ex);
                if (token.IsCancellationRequested) return;

#if DEBUG
                await _dialogService.ShowErrorAsync("Errore", ex.Message);
#else
                // TODO: mostrare errori diversi
                await _dialogService.ShowErrorAsync("Errore", "Si è verificato un errore non previsto");
#endif                
            }
        }
    }
}
