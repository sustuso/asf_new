﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Blazorise;
using Blazorise.DataGrid;

// ReSharper disable once CheckNamespace
namespace System.Linq
{
    public static class BlazoriseExtensions
    {
        private static readonly ConcurrentDictionary<int, PropertyInfo> Properties = new ConcurrentDictionary<int, PropertyInfo>();
        private static readonly MethodInfo ToStringMethod;

        static BlazoriseExtensions()
        {
            ToStringMethod = typeof(object).GetRuntimeMethod(nameof(object.ToString), Array.Empty<Type>());
        }

        public static IQueryable<T> AppyDataGrid<T>(this IQueryable<T> source, DataGridReadDataEventArgs<T> dataGrid, bool paging= true)
        {
            Type type = typeof(T);

            var contains = new List<Expression<Func<T, bool>>>();
            var parameter = Expression.Parameter(type, "p");
            foreach (DataGridColumnInfo column in dataGrid.Columns)
            {
                if (String.IsNullOrWhiteSpace(column.Field)) continue;

                PropertyInfo property = GetProperty(type, column.Field);
                source = source.OrderBy(property, column.Direction);

                if (String.IsNullOrWhiteSpace(column.SearchValue)) continue;

                contains.Add(Contains<T>(parameter, property, column.SearchValue));
            }

            if (contains.Count > 0)
            {
                source = source.Where(contains.AndAlso());
            }

            if (paging)
            {
                source = source.Skip((dataGrid.Page - 1) * dataGrid.PageSize).Take(dataGrid.PageSize);
            }

            return source;
        }

        private static PropertyInfo GetProperty(Type type, string name)
        {
            int key = new {type, name}.GetHashCode();
            return Properties.GetOrAdd(key, v => type.GetProperty(name, BindingFlags.Instance | BindingFlags.IgnoreCase | BindingFlags.Public));
        }

        private static Expression<Func<T, bool>> Contains<T>(ParameterExpression parameter, PropertyInfo property, string text)
        {
            Expression propertyAccess = Expression.MakeMemberAccess(parameter, property);
            if (property.PropertyType != typeof(string))
            {
                propertyAccess = Expression.Call(propertyAccess, ToStringMethod);
            }
            var method = typeof(string).GetRuntimeMethod("Contains", new[] { typeof(string), typeof(StringComparison) });
            var call = Expression.Call(propertyAccess, method, Expression.Constant(text), Expression.Constant(StringComparison.OrdinalIgnoreCase));

            return Expression.Lambda<Func<T, bool>>(call, parameter);
        }

        private static IQueryable<T> OrderBy<T>(this IQueryable<T> source, PropertyInfo property, SortDirection direction)
        {
            if (direction == SortDirection.None) return source;

            string command = direction == SortDirection.Descending ? "OrderByDescending" : "OrderBy";
            var type = typeof(T);

            var parameter = Expression.Parameter(type, "p");
            var propertyAccess = Expression.MakeMemberAccess(parameter, property);
            var orderByExpression = Expression.Lambda(propertyAccess, parameter);
            var resultExpression = Expression.Call(typeof(Queryable), command, new Type[] { type, property.PropertyType },
                source.Expression, Expression.Quote(orderByExpression));
            return source.Provider.CreateQuery<T>(resultExpression);
        }
    }
}
