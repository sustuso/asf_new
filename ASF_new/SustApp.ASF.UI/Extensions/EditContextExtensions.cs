﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SustApp.ASF.DomainModel;
using SustApp.ASF.UI.Components.FormFields;
using Microsoft.AspNetCore.Components.Forms;

namespace Microsoft.AspNetCore.Components.Forms
{
    public static class EditContextExtensions
    {

        public static void AddFormFieldValidation(this EditContext editContext, IEnumerable<FormFieldResult> formFieldResults)
        {
            var messages = new ValidationMessageStore(editContext);

            // Perform object-level validation on request
            editContext.OnValidationRequested += (sender, eventArgs) => ValidateModel(editContext, messages, formFieldResults);

            // Perform per-field validation on each field edit
            editContext.OnFieldChanged += (sender, eventArgs) => ValidateField(editContext, messages, eventArgs.FieldIdentifier);
        }

        private static void ValidateModel(EditContext editContext, ValidationMessageStore messages,
            IEnumerable<FormFieldResult> formFieldResults)
        {
            // Transfer results to the ValidationMessageStore
            messages.Clear();
            foreach (FormFieldResult formFieldResult in formFieldResults)
            {
                var fieldIdentifier = formFieldResult.FormField switch
                {
                    TextFormField _ => FieldIdentifier.Create(RowFormFieldExpression.GetText(formFieldResult)),
                    ComboBoxFormField _ => FieldIdentifier.Create(RowFormFieldExpression.GetText(formFieldResult)),
                    FlagFormField _ => FieldIdentifier.Create(RowFormFieldExpression.GetFlag(formFieldResult)),
                    _ => throw new NotSupportedException()
                };

                ValidateFormFieldResult(fieldIdentifier, messages);
            }

            editContext.NotifyValidationStateChanged();
        }

        private static void ValidateField(EditContext editContext, ValidationMessageStore messages, in FieldIdentifier fieldIdentifier)
        {
            messages.Clear(fieldIdentifier);

            ValidateFormFieldResult(fieldIdentifier, messages);

            // We have to notify even if there were no messages before and are still no messages now,
            // because the "state" that changed might be the completion of some async validation task
            editContext.NotifyValidationStateChanged();
        }

        private static void ValidateFormFieldResult(FieldIdentifier fieldIdentifier, ValidationMessageStore messages)
        {
            RowFormFieldExpression rowFormField = (RowFormFieldExpression)fieldIdentifier.Model;

            switch (rowFormField.Result.FormField)
            {
                case TextFormField tt:
                case ComboBoxFormField cf:
                    if (rowFormField.Result.FormField.IsMandatory && String.IsNullOrWhiteSpace(rowFormField.Result.TextValue))
                    {
                        messages.Add(fieldIdentifier, "Campo obbligatorio");
                    }
                    break;
            }

            // TODO: other validations

        }
    }
}
