﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using IdentityModel;
using IdentityServer4;
using IdentityServer4.Services;
using SustApp.ASF.IdentityProvider.Components;
using SustApp.ASF.IdentityProvider.Data;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace SustApp.ASF.IdentityProvider.Controllers
{
    public class LoginController : Controller
    {
        [HttpGet]
        public IActionResult Index(string returnUrl)
        {
            var viewModel = new LoginViewModel
            {
                ReturnUrl = returnUrl
            };
            return View(viewModel);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> Index(
            LoginViewModel viewModel,
            [FromServices] UserManager<ApplicationUser> userManager,
            [FromServices]IIdentityServerInteractionService interaction)
        {
            var context = await interaction.GetAuthorizationContextAsync(viewModel.ReturnUrl);

            // TODO: gestire il certificato (probabilmente base64) e validarlo
            ApplicationUser user = await userManager.FindByLoginAsync(Constants.SmartCardProvider, viewModel.SmartCard);
            if (user != null)
            {
                var roles = await userManager.GetRolesAsync(user);
                var claims = roles.Select(r => new Claim(JwtClaimTypes.Role, r)).ToArray();

                var props = new AuthenticationProperties
                {
                    // Evitiamo che il cookie di sessione si mantenga nel tempo
                    ExpiresUtc = DateTimeOffset.Now.AddSeconds(5)
                };
                await HttpContext.SignInAsync(user.UserName, props, claims);

                if (String.IsNullOrWhiteSpace(viewModel.ReturnUrl))
                {
                    return Redirect("/");
                }
                else
                {
                    return Redirect(viewModel.ReturnUrl);
                }
            }

            viewModel.Error = "Utente non riconosciuto";
            return View(viewModel);
        }
    }
}