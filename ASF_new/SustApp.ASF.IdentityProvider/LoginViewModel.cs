﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SustApp.ASF.IdentityProvider
{
    public class LoginViewModel
    {
        public string SmartCard { get; set; }

        public string Error { get; set; }

        public bool HasError => !String.IsNullOrWhiteSpace(Error);
        public string ReturnUrl { get; set; }
    }
}
