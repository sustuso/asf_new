﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SustApp.ASF.IdentityProvider.Components
{
    public class UsersCreationOptions
    {
        public List<SmartCardUser> Users { get; } = new List<SmartCardUser>();
    }

    public class SmartCardUser
    {
        public string Username { get; set; }

        public string SmartCard { get; set; }
    }
}
