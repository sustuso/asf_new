﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SustApp.ASF.IdentityProvider.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace SustApp.ASF.IdentityProvider.Components
{
    public class UsersCreationHostedService : IHostedService
    {
        private readonly IServiceProvider _serviceProvider;

        public UsersCreationHostedService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            using IServiceScope scope = _serviceProvider.CreateScope();

            UserManager<ApplicationUser> userManager = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            UsersCreationOptions options = scope.ServiceProvider.GetRequiredService<IOptions<UsersCreationOptions>>().Value;
            var logger = scope.ServiceProvider.GetRequiredService<ILogger<UsersCreationHostedService>>();
            RoleManager<IdentityRole> roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

            // Creazione dei ruoli applicativi
            IdentityRole[] roles = await roleManager.Roles.ToArrayAsync();
            foreach (string role in RoleNames.Roles.Except(roles.Select(r => r.Name)))
            {
                IdentityResult identityResult = await roleManager.CreateAsync(new IdentityRole(role));
                if (!identityResult.Succeeded)
                {
                    logger.LogWarning("Cannot create role {name}: {errors}", role,
                        String.Join(", ", identityResult.Errors.Select(e => e.Description)));
                }
            }

            // Ciclo sugli utenti da creare
            foreach (SmartCardUser smartCardUser in options.Users)
            {
                IdentityResult identityResult;
                // Cerco l'utente
                ApplicationUser user = await userManager.FindByNameAsync(smartCardUser.Username);
                if (user == null)
                {
                    // Creo l'utente perché non esiste
                    user = new ApplicationUser
                    {
                        UserName = smartCardUser.Username
                    };
                    identityResult = await userManager.CreateAsync(user);
                    // Controllo il risultato
                    if (!CheckIdentityResult(identityResult))
                    {
                        // Se c'è un errore salto l'utente
                        continue;
                    }
                }

                // Recupero i login dell'utente
                var logins = await userManager.GetLoginsAsync(user);
                // Cerco il login dedicato alla smartcard
                UserLoginInfo smartCardLogin = logins.FirstOrDefault(l => l.LoginProvider == Constants.SmartCardProvider);
                // Se non c'è il login oppure la chiave è diversa
                if (smartCardLogin == null || smartCardLogin.ProviderKey != smartCardUser.SmartCard)
                {
                    // Rimuovo l'eventuale login esistente
                    if (smartCardLogin != null)
                    {
                        await userManager.RemoveLoginAsync(user, Constants.SmartCardProvider, smartCardLogin.ProviderKey);
                    }
                    // Aggiungo il login per la smartcard
                    identityResult = await userManager.AddLoginAsync(user, new UserLoginInfo(Constants.SmartCardProvider, smartCardUser.SmartCard, Constants.SmartCardProvider));
                    CheckIdentityResult(identityResult);
                }

                // Aggiungo l'utente al ruolo admin
                identityResult = await userManager.AddToRoleAsync(user, RoleNames.Admin);
                CheckIdentityResult(identityResult);

                bool CheckIdentityResult(IdentityResult identityResult)
                {
                    if (!identityResult.Succeeded)
                    {
                        logger.LogWarning("Cannot create user {name}: {errors}", smartCardUser.Username,
                            String.Join(", ", identityResult.Errors.Select(e => e.Description)));
                        return false;
                    }

                    return true;
                }
            }

        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
