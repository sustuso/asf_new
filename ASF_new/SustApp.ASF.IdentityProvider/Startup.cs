using System;
using IdentityServer4.EntityFramework.DbContexts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using IdentityServer4.Configuration;
using IdentityServer4.Extensions;
using SustApp.ASF.IdentityProvider.Components;
using SustApp.ASF.IdentityProvider.Data;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Hosting;
using Microsoft.Net.Http.Headers;
using System.Linq;
using Microsoft.AspNet.OData.Formatter;
using Microsoft.OpenApi.Models;

namespace SustApp.ASF.IdentityProvider
{
    public class Startup
    {
        private IWebHostEnvironment _environment;
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            _environment = environment;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            string connectionString = Configuration.GetConnectionString("Db");
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            X509Certificate2 cert = GetCertificate();

            // Lettura utenze da creare da appsettings.json, sezione Creation
            services.Configure<UsersCreationOptions>(Configuration.GetSection("Creation"));
            // Task in background che crea automaticamente gli utenti di startup
            services.AddHostedService<UsersCreationHostedService>();

            // Gestisce il problema di persistenza del cookie introdotto da .NET Core 3.1
            services.AddSameSiteCookiePolicy();

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(connectionString));

            services.AddIdentityCore<ApplicationUser>()
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>();

            //https://identityserver4.readthedocs.io
            services.AddIdentityServer(o =>
                {
                    o.UserInteraction.LoginUrl = "/login";
                })
                .AddSigningCredential(cert)
                .AddInMemoryIdentityResources(IdentityConfig.GetIdentityResources())
                .AddInMemoryApiResources(IdentityConfig.GetApiResources())
                .AddInMemoryClients(IdentityConfig.GetClients())

                //http://docs.identityserver.io/en/latest/quickstarts/7_entity_framework.html
                //Sostituisce "AddInMemoryIdentityResources" e salva i token/refreshToken nella 
                //tabella "PersistedGrants" di SqlServer. Questo � fiondamentale in caso di bilanciamento macchine:
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = b =>
                        b.UseSqlServer(connectionString,
                            sql => sql.MigrationsAssembly(migrationsAssembly));

                    // this enables automatic token cleanup. this is optional.
                    options.EnableTokenCleanup = true;
                    //options.TokenCleanupInterval = 30; // interval in seconds
                });

            services.AddControllersWithViews();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo()
                {
                    Version = "v1",
                    Title = "API",
                    Description = "Test API with ASP.NET Core 3.0",
                    Contact = new OpenApiContact()
                    {
                        Name = "ASF Detail",
                        Email = "giovanni.ritacco@sustapp.it",
                        Url = new Uri("http://www.sustapp.it/")
                    },
                    License = new OpenApiLicense()
                    {
                        Name = "API SustApp",
                        Url = new Uri("http://www.sustapp.it")
                    },
                });
            });


                services.AddMvcCore(options =>
            {
                foreach (var outputFormatter in options.OutputFormatters.OfType<ODataOutputFormatter>().Where(_ => _.SupportedMediaTypes.Count == 0))
                {
                    outputFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/prs.odatatestxx-odata"));
                }
                foreach (var inputFormatter in options.InputFormatters.OfType<ODataInputFormatter>().Where(_ => _.SupportedMediaTypes.Count == 0))
                {
                    inputFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/prs.odatatestxx-odata"));
                }
            });

        }

        private X509Certificate2 GetCertificate()
        {
            string certificatePassword = Configuration["IdentityProvider:CertificatePassword"];
            if (String.IsNullOrWhiteSpace(certificatePassword))
            {
                throw new InvalidOperationException("No IdentityProvider:CertificatePassword setting specified");
            }

            string certificatePath = Configuration["IdentityProvider:CertificatePath"];
            if (String.IsNullOrWhiteSpace(certificatePath))
            {
                certificatePath = Path.Combine(_environment.ContentRootPath, "CloudKey.pfx");
            }

            //sare una variabile d'ambiente??
            var cert = new X509Certificate2(certificatePath, certificatePassword);
            return cert;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseCors(c => c.WithOrigins("https://localhost:44361").WithHeaders(HeaderNames.ContentType, HeaderNames.Authorization).AllowAnyMethod().AllowCredentials());
            }

            using (var scope = app.ApplicationServices.CreateScope())
            {
                //http://docs.identityserver.io/en/latest/quickstarts/7_entity_framework.html
                //In fase di migrazione, crea la tabella "PersistedGrants" x la gestione dei Token (la prima volta che parte):
                scope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();
            }
            app.UseHttpsRedirection();

            // Gestisce il problema di persistenza del cookie introdotto da .NET Core 3.1
            app.UseCookiePolicy();

            app.UseStaticFiles();
            app.UseRouting();

            // Middleware che forza il redirect verso la pagina di login
            // in caso di /authorize senza prompt
            app.UseMiddleware<AutoLoginMiddleware>();

            app.UseIdentityServer();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                //Il 2� parametro, verra mostrato da Swagger in alto a dx:
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Test API V1");

                //Mostra Swagger nella pagina che apre il WS:
                //Se si commenta la riga sotto -->https://localhost:54846/swagger   json -->  https://localhost:54846/swagger/v1/swagger.json
                //c.RoutePrefix = string.Empty;
            });

        }
    }
}
