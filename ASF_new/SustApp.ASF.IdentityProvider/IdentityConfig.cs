﻿using IdentityServer4.Models;
using System.Collections.Generic;
using System.Security.Claims;
using IdentityModel;
using IdentityServer4;
using Microsoft.Extensions.DependencyInjection;

namespace SustApp.ASF.IdentityProvider
{
    public class IdentityConfig
    {
        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("ASF.Api", "ASF APIs")
                {
                    UserClaims = new List<string> { JwtClaimTypes.Role },
                }
            };
        }

        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                //Flow: ClientCredentials
                new Client
                {
                    ClientId = "NIpJumYfxD",

                    AllowedGrantTypes = GrantTypes.ClientCredentials,

                    ClientSecrets =
                    {
                        new Secret("W$XGbQ6t_QgZB_f&U|7ZCk?_$ijH<{".Sha256())
                    },

                    // scopes that client has access to
                    AllowedScopes = { "ASFApi" }
                },
                new Client
                {
                    ClientId = "ASF.UI",
                    ClientSecrets ={ new Secret("secret")},
                    RequireClientSecret = false,

                    AllowedGrantTypes = GrantTypes.Code,
                    
                    RequirePkce = false,
                    AllowPlainTextPkce = false,

                    //If Authenticate, redirect to "SustApp.ASF.UI.Server" project
                    RedirectUris = { "https://localhost:44361/authentication/login-callback" },
                    //Redirect for logout
                    PostLogoutRedirectUris = { "https://localhost:44361/authentication/logout-callback" },
                    AllowAccessTokensViaBrowser = true,
                    RequireConsent = false,

                    AccessTokenLifetime = 300,
                    AuthorizationCodeLifetime = 60,
                    
                    AlwaysSendClientClaims = true,

                    // scopes that client has access to
                    AllowedScopes ={ IdentityServerConstants.StandardScopes.OpenId, IdentityServerConstants.StandardScopes.Profile, "ASF.Api" }
                }
            };
        }
    }
}
