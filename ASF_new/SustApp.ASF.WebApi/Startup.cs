using System;
using SustApp.ASF.WebApi.Components;
using SustApp.ASF.WebApi.Controllers;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OData.Edm;
using Microsoft.OpenApi.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using IdentityModel;
using IdentityModel.AspNetCore.OAuth2Introspection;
using IdentityServer4.AccessTokenValidation;
using SustApp.ASF.DomainModel.Json;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Microsoft.AspNet.OData.Formatter;

namespace SustApp.ASF.WebApi
{
    public class Startup
    {
        private readonly IEdmModel _edmModel;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            _edmModel = GetEdmModel();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddEFDataLayer(b => b.UseSqlServer(Configuration.GetConnectionString("db")));
            services.AddBiz();
            services.AddOData();

            services.AddHttpContextAccessor();
            services.AddTransient(p => p.GetRequiredService<IHttpContextAccessor>().HttpContext.User);

#if FAKE_AUTH
            // Autenticazione fittizia per avere l'utente impersonificato
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddScheme<AuthenticationSchemeOptions, FakeAuthenticationHandler>(IdentityServerAuthenticationDefaults.AuthenticationScheme, null);

            //add to use Swagger
            services.AddMvcCore(options =>
            {
                foreach (var outputFormatter in options.OutputFormatters.OfType<ODataOutputFormatter>().Where(_ => _.SupportedMediaTypes.Count == 0))
                {
                    outputFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/prs.odatatestxx-odata"));
                }
                foreach (var inputFormatter in options.InputFormatters.OfType<ODataInputFormatter>().Where(_ => _.SupportedMediaTypes.Count == 0))
                {
                    inputFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/prs.odatatestxx-odata"));
                }
            });
#else
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication(options =>
                {
                    //Url IdentityServer (AuthorizationServer)
                    options.Authority = Configuration.GetValue<string>("IdentityProvider");
                    options.ApiName = "SustApp.ASF.Api"; //ApiResourceName in IdentityServer Config file
                    options.NameClaimType = JwtClaimTypes.Subject;
                    options.SupportedTokens = SupportedTokens.Jwt;
                    options.SaveToken = false;
                    options.JwtValidationClockSkew = TimeSpan.FromSeconds(5);

                    // Non ci serve
                    options.EnableCaching = false;
                    
                }
            );
#endif

            // Definizione centralizzata delle policy
            services.AddAuthorization(options =>
                {
                    options.AddPolicy(PolicyNames.Base,
                        policy => policy.RequireAuthenticatedUser().RequireClaim("scope", "ASF.Api"));

                    options.AddPolicy(PolicyNames.Admin,
                        policy => policy.RequireRole(RoleNames.Admin));
                });

            services.AddControllers(o =>
            {
                o.EnableEndpointRouting = false;

                // Richiediamo l'autenticazione su tutti i controller
                o.Filters.Add(new AuthorizeFilter(PolicyNames.Base));
            }).AddNewtonsoftJson(o =>
            {
                o.SerializerSettings.TypeNameHandling = TypeNameHandling.Objects;
                o.SerializerSettings.Converters.Add(new PageResultConverter());
                o.SerializerSettings.SerializationBinder = new SimpleSerializationBinder();
                o.SerializerSettings.ContractResolver = new DeepContractResolver();
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo()
                {
                    Version = "v1",
                    Title = "API",
                    Description = "Test API with ASP.NET Core 3.0",
                    Contact = new OpenApiContact()
                    {
                        Name = "ASF Detail",
                        Email = "giovanni.ritacco@sustapp.it",
                        Url = new Uri("http://www.sustapp.it/")
                    },
                    License = new OpenApiLicense()
                    {
                        Name = "API SustApp",
                        Url = new Uri("http://www.sustapp.it")
                    },
                });

                //Puntamento al file "SSOUsers.xml" per la documentazione dei metodi:
                var xmlFile = $"{System.Reflection.Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = System.IO.Path.Combine(AppContext.BaseDirectory, xmlFile);

                //Aggiunta a Swagger di tali documentazioni:
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseCors(c => c.WithOrigins("https://localhost:44361").WithHeaders(HeaderNames.ContentType, HeaderNames.Authorization).AllowAnyMethod().AllowCredentials());
            }

            app.UseRouting();

            //Redirige le chiamate HTTP in HTTPS 
            app.UseHttpsRedirection();

            //Adds the authentication middleware to the pipeline so authentication will be performed 
            //automatically on every call into the host:
            app.UseAuthentication();

            app.UseAuthorization();
            app.UseMvc(routeBuilder =>
            {
                routeBuilder.EnableDependencyInjection();
                routeBuilder.Select().Filter().OrderBy().Count().Expand().MaxTop(100); //Enable OData operations
                routeBuilder.MapODataServiceRoute("odata", "odata", _edmModel);
            });

            //https://docs.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-3.0&tabs=visual-studio
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                //Il 2° parametro, verra mostrato da Swagger in alto a dx:
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Test API V1");

                //Mostra Swagger nella pagina che apre il WS:
                //Se si commenta la riga sotto -->https://localhost:54846/swagger   json -->  https://localhost:54846/swagger/v1/swagger.json
                //c.RoutePrefix = string.Empty;
            });
        }

        private IEdmModel GetEdmModel()
        {
            var odataBuilder = new ODataConventionModelBuilder();

            return odataBuilder.GetEdmModel();
        }
    }

    public class PageResultConverter : JsonConverter
    {
        private readonly MethodInfo _getItemsMethod;

        public PageResultConverter()
        {
            _getItemsMethod = new Func<PageResult<int>, IEnumerable<int>>(GetItems).GetMethodInfo().GetGenericMethodDefinition();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var pageResult = value as PageResult;

            writer.WriteStartObject();
            writer.WritePropertyName("count");
            writer.WriteValue(pageResult.Count);

            writer.WritePropertyName("items");

            // Get generic type for PageResult<T>
            Type type = pageResult.GetType().GetGenericArguments()[0];
            // Invoke GetItems method with generic type
            var items = _getItemsMethod.MakeGenericMethod(type).Invoke(null, new[] {pageResult});
            serializer.Serialize(writer, items);

            writer.WriteEndObject();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotSupportedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(PageResult).IsAssignableFrom(objectType);
        }

        private static IEnumerable<T> GetItems<T>(PageResult<T> result)
        {
            return result.Items;
        }
    }
}
