﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SustApp.ASF.WebApi.Components
{
    public static class PolicyNames
    {
        public const string Base = "base";
        public const string Admin = "admin";
    }
}
