﻿using SustApp.ASF.Biz;
using SustApp.ASF.DomainModel;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SustApp.ASF.WebApi.Controllers
{
    public class ExamResultController : BaseRestController<ExamResult, Guid>
    {
        public ExamResultController(IBiz<ExamResult, Guid> biz) : base(biz)
        {
        }
    }
}
