﻿using SustApp.ASF.Biz;
using SustApp.ASF.DomainModel;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SustApp.ASF.WebApi.Controllers
{
    public class MilitaryRankController : BaseRestController<MilitaryRank, string>
    {
        public MilitaryRankController(IBiz<MilitaryRank, string> biz) : base(biz)
        {
        }
    }
}
