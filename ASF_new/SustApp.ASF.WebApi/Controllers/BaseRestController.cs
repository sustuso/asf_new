﻿using SustApp.ASF.Biz;
using SustApp.ASF.DomainModel;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SustApp.ASF.WebApi.Components;

namespace SustApp.ASF.WebApi.Controllers
{
    //https://localhost:44383/.well-known/openid-configuration
    [Route("/api/[controller]")]
    public class BaseRestController<T, TKey> : ControllerBase
        where T : class, IEntity<TKey>
    {
        protected IBiz<T, TKey> Biz { get; }

        /// <summary>
        /// CTOR with biz DI
        /// </summary>
        /// <param name="biz"></param>
        public BaseRestController(IBiz<T, TKey> biz)
        {
            Biz = biz;
        }

        /// <summary>
        /// Get all data from repository
        /// </summary>
        /// <returns></returns>
        [RestPaging]
        [HttpGet]
        public virtual IEnumerable<T> Get()
        {
            return Biz;
        }

        /// <summary>
        /// Get single item from repository by PK
        /// </summary>
        /// <returns></returns>
        [RestPaging]
        [HttpGet("{id}")]
        public virtual async Task<ActionResult<T>> Get(TKey id)
        {
            T entity = await Biz.GetAsync(id);
            if (entity == null)
            {
                return NotFound();
            }

            return entity;
        }

        /// <summary>
        /// Adds an entity to the repository
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual async Task<IActionResult> Post([FromBody] T item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await Biz.AddAsync(item);

            return CreatedAtAction("Post", item);
        }

        /// <summary>
        /// Modify an entity in repository
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut]
        public virtual async Task<IActionResult> Put([FromBody] T item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await Biz.UpdateAsync(item);

            return Ok();
        }

        /// <summary>
        /// Delete an entity in repository by PK
        /// </summary>
        /// <param name="id">PK</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public virtual async Task<IActionResult> Delete(TKey id)
        {
            if (id.Equals(default))
            {
                return BadRequest("Id empty");
            }

            await Biz.DeleteAsync(id);

            return Ok();
        }

        /// <summary>
        /// Delete an entity in repository by PK
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public virtual async Task<IActionResult> Delete([FromBody]T entity)
        {
            await Biz.DeleteAsync(entity);

            return Ok();
        }
    }
}
