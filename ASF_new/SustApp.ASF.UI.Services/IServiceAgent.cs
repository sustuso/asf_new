﻿using SustApp.ASF.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SustApp.ASF.UI.Services
{
    public interface IServiceAgent<T, in TKey>
        where T : class, IEntity<TKey>
    {
        Task<T> AddAsync(T entity, CancellationToken token);

        // Task<T> AddAsync(T entity) => AddAsync(entity, CancellationToken.None);

        Task<T> UpdateAsync(T entity, CancellationToken token);

        Task DeleteAsync(T entity, CancellationToken token);

        Task<T> GetAsync(TKey id, CancellationToken token);

        Task<CountResult<T>> GetAsync(CancellationToken token) => GetAsync<T>(null, token);

        Task<CountResult<T>> GetAsync(GetRequest<T, T> query, CancellationToken token) =>
            GetAsync<T>(query, token);

        Task<CountResult<TResult>> GetAsync<TResult>(GetRequest<T, TResult> query,
            CancellationToken token)
            where TResult : class;
    }
}
