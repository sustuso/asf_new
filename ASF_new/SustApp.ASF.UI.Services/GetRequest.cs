﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SustApp.ASF.UI.Services
{
    public class GetRequest<T, TResult>
    {
        public Func<IQueryable<T>, IQueryable<TResult>> QueryFactory { get; set; }

        public bool WithCount { get; set; }

        public List<Expression<Func<T, object>>> ExpandExpressions { get; } = new List<Expression<Func<T, object>>>();
    }

    public class GetRequest<T> : GetRequest<T, T>
    {
    }
}