using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using SustApp.ASF.DomainModel;
using SustApp.ASF.UI.Server.Components;
using SustApp.ASF.UI.ServiceAgent;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;

namespace SustApp.ASF.UI.Server
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddHttpClient();
            services.AddTransient(p => p.GetRequiredService<IHttpClientFactory>().CreateClient());

            // Autenticazione fittizia per permette a Blazor di vedere un utente attivo
            services.AddAuthentication("fake").AddScheme<AuthenticationSchemeOptions, FakeAuthenticationHandler>("fake", null);
            // Provider fake di access token
            services.AddTransient<IAccessTokenProvider, FakeAccessTokenProvider>();

            // Register Blazor app dependencies
            UI.Program.ConfigureServices(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            //app.UseBlazorFrameworkFiles();
            // Non funziona pi� UseBlazorFrameworkFiles perci� esponeniamo direttamente i file del progetto UI
            string uiwwroot = Path.Combine(env.ContentRootPath, "..\\", "SustApp.ASF.UI", "wwwroot");
            string uidist = Path.Combine(env.ContentRootPath, "..\\", "SustApp.ASF.UI", "bin\\Debug\\netstandard2.1\\dist");
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new CompositeFileProvider(new PhysicalFileProvider(uidist), new PhysicalFileProvider(uiwwroot))
            });

            app.UseRouting();

            app.UseAuthentication();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }

    }
}
