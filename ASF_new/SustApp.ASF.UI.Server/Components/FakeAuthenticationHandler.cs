﻿using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace SustApp.ASF.UI.Server.Components
{
    /// <summary>
    /// TO DO: Da sostituire il codice, nel momento in cui su Blazor, implementeranno l'autenticazione con OAUTH.
    /// </summary>
    public class FakeAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        public FakeAuthenticationHandler(IOptionsMonitor<AuthenticationSchemeOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock) : base(options, logger, encoder, clock)
        {
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            // Impersonificazione di un finto utente
            var identity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, "user1fake"),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, "admin"),
            }, "fake");

            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal, identity.AuthenticationType);
            return AuthenticateResult.Success(ticket);
        }
    }
}
