using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;

namespace SustApp.ASF.UI.Server.Components
{
    public class FakeAccessTokenProvider : IAccessTokenProvider
    {
        public async ValueTask<AccessTokenResult> RequestAccessToken()
        {
            return new AccessTokenResult(AccessTokenResultStatus.Success, new AccessToken
            {
                Value = "test"
            }, "");
        }

        public ValueTask<AccessTokenResult> RequestAccessToken(AccessTokenRequestOptions options)
        {
            throw new NotImplementedException();
        }
    }
}