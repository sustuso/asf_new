﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SustApp.ASF.Dal.EF.Converters;
using SustApp.ASF.DomainModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SustApp.ASF.Dal.EF
{
    public class ASFDbContext : DbContext
    {
        public ASFDbContext(DbContextOptions<ASFDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            modelBuilder.Entity<FormField>(e =>
            {
                e.ToTable("FormFields");
                e.Property(p => p.Id).HasDefaultValueSql("newsequentialid()");
                e.HasDiscriminator("Discriminator", typeof(string));
                e.Property("Discriminator").HasMaxLength(50);
                e.HasIndex("Discriminator");
                e.Property(p => p.Version).IsRowVersion();
            });
            modelBuilder.Entity<TextFormField>(e =>
            {
                e.Property(p => p.IsMultiline).HasColumnName("TextIsMultiline");
                e.Property(p => p.Size).HasColumnName("TextSize");
                e.Property(p => p.ValidationRegex).HasColumnName("TextValidationRegex");
                e.Property(p => p.ValidationRegexErrorMessage).HasColumnName("TextValidationRegexErrorMessage");
            });
            modelBuilder.Entity<NumberFormField>(e =>
            {
                e.Property(p => p.Decimals).HasColumnName("NumberDecimals");
                e.Property(p => p.MinimumValue).HasColumnName("NumberMinimumValue");
                e.Property(p => p.MaximumValue).HasColumnName("NumberMaximumValue");
            });
            modelBuilder.Entity<FlagFormField>();
            modelBuilder.Entity<DateFormField>(e =>
            {
                e.Property(p => p.Type).HasColumnName("DateType");
            });
            modelBuilder.Entity<ComboBoxFormField>(e =>
            {
                e.Property(p => p.Items)
                    .HasColumnName("ComboBoxItems")
                    .HasConversion(new JsonConverter<List<string>>());
            });

            modelBuilder.Entity<Audit>(e =>
            {
                e.ToTable("Audits");
                e.Property(p => p.Id).HasDefaultValueSql("newsequentialid()");
                e.Property(p => p.User);
                e.Property(p => p.Message);
                e.Property(p => p.EntityId);
                e.Property(p => p.EntityName);
            });

            modelBuilder.Entity<Site>(e =>
            {
                e.ToTable("Sites");
                e.Property(p => p.Id).HasDefaultValueSql("newsequentialid()");
            });

            modelBuilder.Entity<Exam>(e =>
            {
                e.ToTable("Exams");
                e.Property(p => p.Id).HasDefaultValueSql("newsequentialid()");
                e.Property(p => p.Version).IsRowVersion();
            });

            modelBuilder.Entity<ExamResult>(e =>
            {
                e.ToTable("ExamResults");
                e.Property(p => p.Id).HasDefaultValueSql("newsequentialid()");
                e.Property(p => p.Version).IsRowVersion();
                e.HasOne(p => p.Exam).WithMany().HasForeignKey(p => p.ExamId).OnDelete(DeleteBehavior.NoAction);
            });

            modelBuilder.Entity<FormFieldResult>(e =>
            {
                e.ToTable("FormFieldResults");
                e.Property(p => p.Id).HasDefaultValueSql("newsequentialid()");
                e.Property(p => p.Version).IsRowVersion();
                e.HasOne(p => p.FormField).WithMany().HasForeignKey(p => p.FormFieldId).OnDelete(DeleteBehavior.NoAction);
                e.HasOne(p => p.ExamResult).WithMany(p => p.FormFields).HasForeignKey(p => p.ExamResultId).OnDelete(DeleteBehavior.NoAction);
            });

            modelBuilder.Entity<MeasureUnit>(e =>
            {
                e.ToTable("MeasureUnits");
            });

            modelBuilder.Entity<Measurement>(e =>
            {
                e.ToTable("Measurements");
            });

            modelBuilder.Entity<MedicalSpecialization>(e =>
            {
                e.ToTable("MedicalSpecializations");
            });

            modelBuilder.Entity<MilitaryRank>(e =>
            {
                e.ToTable("MilitaryRanks");
            });

            modelBuilder.Entity<City>(e =>
            {
                e.ToTable("Cities");
            });

            modelBuilder.Entity<Person>(e =>
            {
                e.ToTable("Persons");
                e.Property(p => p.Id).HasDefaultValueSql("newsequentialid()");
                //e.HasOne(p => p.Exam).WithMany().HasForeignKey(p => p.ExamId).OnDelete(DeleteBehavior.NoAction);
                e.HasMany(p => p.Addresses).WithOne().HasForeignKey(p => p.IdPerson).OnDelete(DeleteBehavior.NoAction);
                e.HasMany(p => p.Contacts).WithOne().HasForeignKey(p => p.IdPerson).OnDelete(DeleteBehavior.NoAction);
            });


        }

    }
}
