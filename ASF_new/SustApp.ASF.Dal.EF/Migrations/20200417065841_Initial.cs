﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SustApp.ASF.Dal.EF.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Audits",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    AuditDate = table.Column<DateTimeOffset>(nullable: false),
                    User = table.Column<string>(maxLength: 100, nullable: false),
                    Message = table.Column<string>(nullable: false),
                    EntityName = table.Column<string>(maxLength: 100, nullable: false),
                    EntityId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Audits", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Comune = table.Column<string>(nullable: true),
                    Provincia = table.Column<string>(nullable: true),
                    Regione = table.Column<string>(nullable: true),
                    Prefisso = table.Column<string>(nullable: true),
                    CAP = table.Column<string>(nullable: true),
                    CodFisco = table.Column<string>(nullable: true),
                    Abitanti = table.Column<string>(nullable: true),
                    Link = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MeasureUnits",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 50, nullable: false),
                    Name = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MeasureUnits", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MedicalSpecializations",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 50, nullable: false),
                    Name = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MedicalSpecializations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MilitaryRanks",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 50, nullable: false),
                    Name = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MilitaryRanks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Persons",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    IdASFCompany = table.Column<Guid>(nullable: false),
                    Code = table.Column<string>(maxLength: 10, nullable: true),
                    IdCompany = table.Column<Guid>(nullable: false),
                    DateInsert = table.Column<DateTime>(nullable: false),
                    DateDel = table.Column<DateTime>(nullable: true),
                    DateModify = table.Column<DateTime>(nullable: true),
                    UserInsert = table.Column<Guid>(nullable: true),
                    UserDel = table.Column<Guid>(nullable: true),
                    UserModify = table.Column<Guid>(nullable: true),
                    Enable = table.Column<bool>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Surname = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<DateTime>(nullable: true),
                    IdPlaceOfBirth = table.Column<Guid>(nullable: true),
                    IdCountry = table.Column<Guid>(nullable: true),
                    IdLanguage = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Persons", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sites",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sites", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Measurements",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 50, nullable: false),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    MeasureUnitId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Measurements", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Measurements_MeasureUnits_MeasureUnitId",
                        column: x => x.MeasureUnitId,
                        principalTable: "MeasureUnits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IdPerson = table.Column<Guid>(nullable: false),
                    Street = table.Column<string>(maxLength: 50, nullable: false),
                    CityId = table.Column<string>(nullable: false),
                    AddressType = table.Column<int>(nullable: false),
                    Default = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Addresses_Cities_CityId",
                        column: x => x.CityId,
                        principalTable: "Cities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Addresses_Persons_IdPerson",
                        column: x => x.IdPerson,
                        principalTable: "Persons",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Contacts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IdPerson = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    ContactType = table.Column<int>(nullable: false),
                    Default = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contacts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Contacts_Persons_IdPerson",
                        column: x => x.IdPerson,
                        principalTable: "Persons",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Exams",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Version = table.Column<byte[]>(rowVersion: true, nullable: true),
                    Description = table.Column<string>(maxLength: 200, nullable: false),
                    MeasurementId = table.Column<string>(nullable: false),
                    MedicalSpecializationId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Exams", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Exams_Measurements_MeasurementId",
                        column: x => x.MeasurementId,
                        principalTable: "Measurements",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Exams_MedicalSpecializations_MedicalSpecializationId",
                        column: x => x.MedicalSpecializationId,
                        principalTable: "MedicalSpecializations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExamResults",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Version = table.Column<byte[]>(rowVersion: true, nullable: true),
                    Date = table.Column<DateTimeOffset>(nullable: false),
                    ExamId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExamResults", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExamResults_Exams_ExamId",
                        column: x => x.ExamId,
                        principalTable: "Exams",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "FormFields",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Version = table.Column<byte[]>(rowVersion: true, nullable: true),
                    Label = table.Column<string>(maxLength: 200, nullable: false),
                    Placeholder = table.Column<string>(maxLength: 500, nullable: false),
                    HelpText = table.Column<string>(nullable: false),
                    IsMandatory = table.Column<bool>(nullable: false),
                    ParentFormFieldId = table.Column<Guid>(nullable: true),
                    ExamId = table.Column<Guid>(nullable: false),
                    Discriminator = table.Column<string>(maxLength: 50, nullable: false),
                    ComboBoxItems = table.Column<string>(nullable: true),
                    DateType = table.Column<int>(nullable: true),
                    NumberDecimals = table.Column<int>(nullable: true),
                    NumberMinimumValue = table.Column<double>(nullable: true),
                    NumberMaximumValue = table.Column<double>(nullable: true),
                    TextSize = table.Column<int>(nullable: true),
                    TextIsMultiline = table.Column<bool>(nullable: true),
                    TextValidationRegexErrorMessage = table.Column<string>(maxLength: 500, nullable: true),
                    TextValidationRegex = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FormFields", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FormFields_Exams_ExamId",
                        column: x => x.ExamId,
                        principalTable: "Exams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FormFields_FormFields_ParentFormFieldId",
                        column: x => x.ParentFormFieldId,
                        principalTable: "FormFields",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FormFieldResults",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Version = table.Column<byte[]>(rowVersion: true, nullable: true),
                    TextValue = table.Column<string>(nullable: true),
                    NumberValue = table.Column<double>(nullable: true),
                    FlagValue = table.Column<bool>(nullable: true),
                    DateValue = table.Column<DateTime>(nullable: true),
                    FormFieldId = table.Column<Guid>(nullable: false),
                    ExamResultId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FormFieldResults", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FormFieldResults_ExamResults_ExamResultId",
                        column: x => x.ExamResultId,
                        principalTable: "ExamResults",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_FormFieldResults_FormFields_FormFieldId",
                        column: x => x.FormFieldId,
                        principalTable: "FormFields",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_CityId",
                table: "Addresses",
                column: "CityId");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_IdPerson",
                table: "Addresses",
                column: "IdPerson");

            migrationBuilder.CreateIndex(
                name: "IX_Contacts_IdPerson",
                table: "Contacts",
                column: "IdPerson");

            migrationBuilder.CreateIndex(
                name: "IX_ExamResults_ExamId",
                table: "ExamResults",
                column: "ExamId");

            migrationBuilder.CreateIndex(
                name: "IX_Exams_MeasurementId",
                table: "Exams",
                column: "MeasurementId");

            migrationBuilder.CreateIndex(
                name: "IX_Exams_MedicalSpecializationId",
                table: "Exams",
                column: "MedicalSpecializationId");

            migrationBuilder.CreateIndex(
                name: "IX_FormFieldResults_ExamResultId",
                table: "FormFieldResults",
                column: "ExamResultId");

            migrationBuilder.CreateIndex(
                name: "IX_FormFieldResults_FormFieldId",
                table: "FormFieldResults",
                column: "FormFieldId");

            migrationBuilder.CreateIndex(
                name: "IX_FormFields_Discriminator",
                table: "FormFields",
                column: "Discriminator");

            migrationBuilder.CreateIndex(
                name: "IX_FormFields_ExamId",
                table: "FormFields",
                column: "ExamId");

            migrationBuilder.CreateIndex(
                name: "IX_FormFields_ParentFormFieldId",
                table: "FormFields",
                column: "ParentFormFieldId");

            migrationBuilder.CreateIndex(
                name: "IX_Measurements_MeasureUnitId",
                table: "Measurements",
                column: "MeasureUnitId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Addresses");

            migrationBuilder.DropTable(
                name: "Audits");

            migrationBuilder.DropTable(
                name: "Contacts");

            migrationBuilder.DropTable(
                name: "FormFieldResults");

            migrationBuilder.DropTable(
                name: "MilitaryRanks");

            migrationBuilder.DropTable(
                name: "Sites");

            migrationBuilder.DropTable(
                name: "Cities");

            migrationBuilder.DropTable(
                name: "Persons");

            migrationBuilder.DropTable(
                name: "ExamResults");

            migrationBuilder.DropTable(
                name: "FormFields");

            migrationBuilder.DropTable(
                name: "Exams");

            migrationBuilder.DropTable(
                name: "Measurements");

            migrationBuilder.DropTable(
                name: "MedicalSpecializations");

            migrationBuilder.DropTable(
                name: "MeasureUnits");
        }
    }
}
