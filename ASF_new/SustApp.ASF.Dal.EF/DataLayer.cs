﻿using SustApp.ASF.DomainModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SustApp.ASF.Dal.EF
{
    public class DataLayer<T, TKey> : IDataLayer<T, TKey>
        where T : class, IEntity<TKey>
    {
        private readonly ASFDbContext _context;

        public DataLayer(ASFDbContext context)
        {
            _context = context;
        }

        public DbSet<T> Set => _context.Set<T>();

        #region IQuerable Interface

        IEnumerator<T> IEnumerable<T>.GetEnumerator() => Query.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => Query.GetEnumerator();

        Type IQueryable.ElementType => Query.ElementType;

        Expression IQueryable.Expression => Query.Expression;

        IQueryProvider IQueryable.Provider => Query.Provider;

        private IQueryable<T> Query => Set;

        #endregion


        #region CRUD Operations

        /// <summary>
        /// Add T entity in database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task AddAsync(T entity)
        {
            try
            {
                //entity.Id = default; //GiovanniR rimosso per eliminare l'errore di inserimento //todo verificare se corretto

                _context.Entry(entity).State = EntityState.Added;
                await _context.SaveChangesAsync();
            }
            catch (DBConcurrencyException e)
            {
                throw new SustAppVersionException(entity, e);
            }
            catch (DbUpdateException e)
            {
                throw new SustAppEntityException(entity, $"An error has occurred while adding entity {entity.Id}", e);
            }
        }

        /// <summary>
        /// Update T entity in database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task UpdateAsync(T entity)
        {
            try
            {
                _context.Entry(entity).State = EntityState.Modified;

                await _context.SaveChangesAsync();
            }
            catch (DBConcurrencyException e)
            {
                throw new SustAppVersionException(entity, e);
            }
            catch (DbUpdateException e)
            {
                throw new SustAppEntityException(entity, $"An error has occurred while updating entity {entity.Id}", e);
            }
        }

        /// <summary>
        /// Delete an entity by PK with RowVersion for optimistic concurrency
        /// </summary>
        /// <param name="id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public async Task DeleteAsync(TKey id, byte[] version)
        {
            T entity = await Set.FindAsync(id);
            if (entity is IVersionedEntity<TKey> v)
            {
                v.Version = version;
            }

            try
            {
                _context.Entry(entity).State = EntityState.Deleted;
                await _context.SaveChangesAsync();
            }
            catch (DBConcurrencyException e)
            {
                throw new SustAppVersionException(entity, e);
            }
            catch (DbUpdateException e)
            {
                throw new SustAppEntityException(entity, $"An error has occurred while deleting entity {entity.Id}", e);
            }
        }

        /// <summary>
        /// Delete an entity with RowVersion for optimistic concurrency
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task DeleteAsync(T entity)
        {
            try
            {
                _context.Entry(entity).State = EntityState.Deleted;
                await _context.SaveChangesAsync();
            }
            catch (DBConcurrencyException e)
            {
                throw new SustAppVersionException(entity, e);
            }
            catch (DbUpdateException e)
            {
                throw new SustAppEntityException(entity, $"An error has occurred while deleting entity {entity.Id}", e);
            }
        }

        /// <summary>
        /// Get a single enity by PK
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<T> GetAsync(TKey id)
        {
            return await Set.FindAsync(id);
        }

        #endregion
    }
}