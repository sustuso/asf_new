﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace SustApp.ASF.Dal.EF.Converters
{
    internal class UriConverter : ValueConverter<Uri, string>
    {
        public UriConverter() : base(e => e.ToString(), e => new Uri(e))
        {
        }
    }
}
