﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Text.Json;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace SustApp.ASF.Dal.EF.Converters
{
    internal class JsonConverter<T> : ValueConverter<T, string>
    {
        public JsonConverter() : base(e => JsonSerializer.Serialize(e, null), e => JsonSerializer.Deserialize<T>(e, null))
        {
        }
    }
}
