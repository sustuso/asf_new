﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace SustApp.ASF.Dal.EF
{
    /// <summary>
    /// Factory class for give the connection string to EF Core, since this layer is not a sturtup project of the solution
    /// </summary>
    internal class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<ASFDbContext>
    {
        /// <summary>
        /// Using by EF Core, to retrive the connection string
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public ASFDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ASFDbContext>();
            optionsBuilder.UseSqlServer(@"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=ASF;Integrated Security=SSPI");

            return new ASFDbContext(optionsBuilder.Options);
        }
    }
}
