﻿using System;
using System.Collections.Generic;
using System.Text;
using SustApp.ASF.Dal;
using SustApp.ASF.Dal.EF;
using SustApp.ASF.DomainModel;
using Microsoft.EntityFrameworkCore;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddEFDataLayer(this IServiceCollection services, Action<DbContextOptionsBuilder> optionsBuilder)
        {
            services.AddHostedService<MigrationsService>();
            services.AddDbContext<ASFDbContext>(optionsBuilder);

            services.AddDataLayer<Site, Guid>();
            services.AddDataLayer<Audit, Guid>();
            services.AddDataLayer<FormField, Guid>();
            services.AddDataLayer<Measurement, string>();
            services.AddDataLayer<MeasureUnit, string>();
            services.AddDataLayer<MedicalSpecialization, string>();
            services.AddDataLayer<Exam, Guid>();
            services.AddDataLayer<FormFieldResult, Guid>();
            services.AddDataLayer<ExamResult, Guid>();
            services.AddDataLayer<MilitaryRank, string>();
            services.AddDataLayer<City, string>();
            services.AddDataLayer<Person, Guid>();

            return services;
        }

        private static IServiceCollection AddDataLayer<T, TKey>(this IServiceCollection services)
            where T : class, IEntity<TKey>
        {
            return services.AddTransient<IDataLayer<T, TKey>, DataLayer<T, TKey>>();
        }
    }
}
