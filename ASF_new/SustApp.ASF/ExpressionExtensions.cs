﻿using System;
using System.Collections.Generic;
using System.Reflection;

// ReSharper disable once CheckNamespace
namespace System.Linq.Expressions
{
    /// <summary>
    /// Extensions for <see cref="Expression"/>
    /// </summary>
    public static class ExpressionExtensions
    {
        /// <summary>
        /// Sostituisce l'espressione all'interno di una principale con un'altra specificata
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static T Replace<T>(this T expression, Expression from, Expression to)
            where T : Expression
        {
            return expression.Replace(e => e == from, e => to);
        }

        /// <summary>
        /// Visita un'espressione semplicemente navigandola
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="visit"></param>
        /// <returns></returns>
        public static void Visit(this Expression expression, Action<Expression> visit)
        {
            new ReplaceVisitor(e => { visit(e); return false; }, null, false).Visit(expression);
        }

        /// <summary>
        /// Visita un'espressione restituendone un IEnumerable
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static IEnumerable<Expression> Visit(this Expression expression)
        {
            var result = new List<Expression>();
            Visit(expression, e => result.Add(e));

            return result;
        }

        /// <summary>
        /// Restituisce la prima espressione il cui tipo restituisce il valore cercato. Se necessario compila e valuta l'espressione
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="expressions"></param>
        /// <returns></returns>
        public static TValue GetConstantValue<TValue>(this IEnumerable<Expression> expressions)
        {
            foreach (Expression e in expressions)
            {
                if (e.NodeType != ExpressionType.Extension && typeof(TValue).IsAssignableFrom(e.Type))
                {
                    if (e is ConstantExpression ce) return (TValue)ce.Value;

                    var lambda = Expression.Lambda<Func<TValue>>(e);
                    return lambda.Compile()();
                }
            }

            return default(TValue);
        }

        /// <summary>
        /// Sostituisce l'espressione all'interno di una principale in base alle regole di match
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="match"></param>
        /// <param name="replaceWith"></param>
        /// <returns></returns>
        public static T Replace<T>(this T expression, Func<Expression, bool> match, Func<Expression, Expression> replaceWith)
            where T : Expression
        {
            return Replace(expression, match, replaceWith, false);
        }

        /// <summary>
        /// Sostituisce l'espressione all'interno di una principale in base alle regole di match
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="match"></param>
        /// <param name="replaceWith"></param>
        /// <param name="visitReplaced"></param>
        /// <returns></returns>
        public static T Replace<T>(this T expression, Func<Expression, bool> match, Func<Expression, Expression> replaceWith, bool visitReplaced)
            where T : Expression
        {
            return (T)new ReplaceVisitor(match, replaceWith, visitReplaced).Visit(expression);
        }

        /// <summary>
        /// Ottimizza l'espressioni semplificando le logiche AND e OR
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static T Optimize<T>(this T expression)
            where T : Expression
        {
            return (T)new OptimizeVisitor().Visit(expression);
        }

        /// <summary>
        /// Gets the member used in the expression
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="property"></param>
        /// <returns></returns>
        public static MemberInfo GetMemberInfo<T, TValue>(this Expression<Func<T, TValue>> property)
        {
            IEnumerable<Expression> expressions = property.Body.Visit();
            var memberExpression = expressions.OfType<MemberExpression>().FirstOrDefault();
            if (memberExpression == null)
            {
                // Try with a method
                var methodCallExpression = expressions.OfType<MethodCallExpression>().FirstOrDefault();
                if (methodCallExpression == null)
                    throw new ArgumentException("Please specificy a property");

                return methodCallExpression.Method;
            }

            return memberExpression.Member;
        }

        /// <summary>
        /// Compose 2 lambda expression using a specific binary operation
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="binaryOperation">es: Expression.Or, Expression.And</param>
        /// <returns></returns>
        public static Expression<Func<T, bool>> Compose<T>(this Expression<Func<T, bool>> a, Expression<Func<T, bool>> b,
            Func<Expression, Expression, BinaryExpression> binaryOperation
            )
        {
            if (a == null)
                return b;
            else if (b == null)
                return a;
            else
            {
                ParameterExpression p = a.Parameters[0];

                SubstExpressionVisitor visitor = new SubstExpressionVisitor();
                visitor.subst[b.Parameters[0]] = p;

                Expression body = binaryOperation(a.Body, visitor.Visit(b.Body));
                return Expression.Lambda<Func<T, bool>>(body, p);
            }
        }

        public static Expression<Func<T, bool>> Compose<T>(
            Func<Expression, Expression, BinaryExpression> binaryOperation,
            params Expression<Func<T, bool>>[] expressions
            )
        {
            return Compose(binaryOperation, expressions.AsEnumerable());
        }

        public static Expression<Func<T, bool>> Compose<T>(
            Func<Expression, Expression, BinaryExpression> binaryOperation,
            IEnumerable<Expression<Func<T, bool>>> expressions
            )
        {
            // se non ci sono espressioni non ritorna NULL
            if (expressions == null)
                return null;

            // Elimino espressioni nulle e verifico se ne esistono di valide
            expressions = expressions.Where(exp => exp != null);
            if (expressions.Count() == 0)
                return null;

            if (expressions.Count() == 1)
                // Se c'é un solo elemento la composizione non produce alcun risultato e torno l'elemento stesso
                return expressions.First();
            else
            {
                var firstExpression = expressions.First();
                var otherExpressions = expressions.Skip(1).Select(e => e.Replace(e.Parameters[0], firstExpression.Parameters[0])).ToArray();
                // Combino il primo elemento 0 con la parte restante delle espressioni combinate ricorsivamente (b)
                var b = Compose(binaryOperation, otherExpressions);
                return b.Compose(firstExpression, binaryOperation);
            }
        }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> a, params Expression<Func<T, bool>>[] others)
        {
            return Or(new[] { a }.Union(others));
        }

        public static Expression<Func<T, bool>> Or<T>(this IEnumerable<Expression<Func<T, bool>>> expressions)
        {
            return Compose(Expression.Or, expressions);
        }

        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> a, params Expression<Func<T, bool>>[] others)
        {
            return And(new[] { a }.Union(others));
        }

        public static Expression<Func<T, bool>> And<T>(this IEnumerable<Expression<Func<T, bool>>> expressions)
        {
            return Compose(Expression.And, expressions);
        }

        public static Expression<Func<T, bool>> OrElse<T>(this Expression<Func<T, bool>> a, params Expression<Func<T, bool>>[] others)
        {
            return OrElse(new[] { a }.Union(others));
        }

        public static Expression<Func<T, bool>> OrElse<T>(this IEnumerable<Expression<Func<T, bool>>> expressions)
        {
            // Escludo automaticamente le espressioni già false
            Expression<Func<T, bool>>[] allExpressions = expressions.Reverse().ToArray();
            Expression<Func<T, bool>>[] validExpressions = allExpressions.Where(e => !(e.Body is ConstantExpression ce && ce.Value.Equals(false))).ToArray();

            // Se sono presenti solo condizioni a false, restituisco direttamente false
            if (allExpressions.Length > 0 && validExpressions.Length == 0)
            {
                return Expression.Lambda<Func<T, bool>>(Expression.Constant(false), Expression.Parameter(typeof(T)));
            }

            // Potrebbe restituire null se non ci sono espressioni
            return Compose(Expression.OrElse, validExpressions);
        }

        public static Expression<Func<T, bool>> AndAlso<T>(this Expression<Func<T, bool>> a, params Expression<Func<T, bool>>[] others)
        {
            return AndAlso(new[] { a }.Union(others).ToArray());
        }

        public static Expression<Func<T, bool>> AndAlso<T>(this IEnumerable<Expression<Func<T, bool>>> expressions)
        {
            return Compose(Expression.AndAlso, expressions.Reverse());
        }

        private class SubstExpressionVisitor : System.Linq.Expressions.ExpressionVisitor
        {
            public Dictionary<Expression, Expression> subst = new Dictionary<Expression, Expression>();

            protected override Expression VisitParameter(ParameterExpression node)
            {
                Expression newValue;
                if (subst.TryGetValue(node, out newValue))
                {
                    return newValue;
                }
                else
                {
                    return node;
                }
            }
        }

        private class OptimizeVisitor : ExpressionVisitor
        {
            protected override Expression VisitBinary(BinaryExpression node)
            {
                var baseNode = (BinaryExpression)base.VisitBinary(node);

                var leftTrue = ConstantEqualsTo(baseNode.Left, true);
                var rightTrue = ConstantEqualsTo(baseNode.Right, true);
                var leftFalse = ConstantEqualsTo(baseNode.Left, false);
                var rightFalse = ConstantEqualsTo(baseNode.Right, false);
                switch (baseNode.NodeType)
                {
                    case ExpressionType.AndAlso:
                    case ExpressionType.And:
                        if (leftTrue && rightTrue)
                            return Expression.Constant(true);
                        if (leftFalse || rightFalse)
                            return Expression.Constant(false);
                        if (leftTrue)
                            return baseNode.Right;
                        if (rightTrue)
                            return baseNode.Left;
                        break;
                    case ExpressionType.Or:
                    case ExpressionType.OrElse:
                        if (leftTrue || rightTrue)
                            return Expression.Constant(true);
                        if (leftFalse && rightFalse)
                            return Expression.Constant(false);
                        if (leftFalse)
                            return baseNode.Right;
                        if (rightFalse)
                            return baseNode.Left;
                        break;
                }
                return baseNode;
            }

            private bool ConstantEqualsTo<T>(Expression expression, T value)
            {
                var constantExpression = expression as ConstantExpression;
                if (constantExpression?.Value is T)
                    return EqualityComparer<T>.Default.Equals((T)constantExpression.Value, value);

                return false;
            }
        }

        private class ReplaceVisitor : ExpressionVisitor
        {
            private readonly Func<Expression, bool> _match;
            private readonly Func<Expression, Expression> _replaceWith;
            private readonly bool _visitReplaced;

            protected internal ReplaceVisitor(Func<Expression, bool> match, Func<Expression, Expression> replaceWith, bool visitReplaced)
            {
                _match = match;
                _replaceWith = replaceWith;
                _visitReplaced = visitReplaced;
            }

            public override Expression Visit(Expression node)
            {
                if (node == null || node.NodeType == (ExpressionType)10000) return node;

                if (_match(node))
                {
                    Expression originalNode = node;
                    node = _replaceWith(node);
                    if (_visitReplaced) node = base.Visit(node);
                    return node;
                }
                return base.Visit(node);
            }
        }
    }
}
