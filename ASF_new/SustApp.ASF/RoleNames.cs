﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SustApp.ASF
{
    public static class RoleNames
    {
        public static IReadOnlyList<string> Roles => new[] {Admin};

        public const string Admin = "admin";
    }
}
