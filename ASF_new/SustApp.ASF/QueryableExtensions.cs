﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using LinqKit;

namespace System.Linq
{
    public static class QueryableExtensions
    {

        private static readonly MethodInfo ToStringMethod;
        private static readonly MethodInfo ContainsMethod;

        static QueryableExtensions()
        {
            ToStringMethod = typeof(object).GetRuntimeMethod(nameof(object.ToString), Array.Empty<Type>());
            ContainsMethod = typeof(string).GetRuntimeMethod("Contains", new[] { typeof(string) });
        }

        public static IQueryable<TEntity> WhereAnyProperty<TEntity>(this IQueryable<TEntity> source, string text)
        {
            if (String.IsNullOrWhiteSpace(text)) return source;

            var type = typeof(TEntity);
            var expression = PredicateBuilder.New<TEntity>();
            foreach (var property in type.GetRuntimeProperties())
            {
                // p =>
                var parameter = Expression.Parameter(type, "p");
                // p.Proprieta
                Expression propertyAccess = Expression.MakeMemberAccess(parameter, property);
                Expression resultExpression;
                if (property.PropertyType == typeof(string))
                {
                    // p.Proprieta.Contains(text)
                    resultExpression = Expression.Call(propertyAccess, ContainsMethod, Expression.Constant(text));
                }
                else if (property.PropertyType.IsClass || property.PropertyType == typeof(Guid))
                {
                    continue;
                }
                else
                {
                    var convertedValue = ConvertValue(text, property.PropertyType);
                    if (convertedValue == null) continue;

                    // p.Proprieta == text
                    resultExpression = Expression.Equal(propertyAccess, Expression.Constant(convertedValue));
                }

                var lambda = Expression.Lambda<Func<TEntity, bool>>(resultExpression, parameter);
                expression = expression.Or(lambda);
            }

            return source.Where(expression);
        }

        private static object ConvertValue(string text, Type type)
        {
            try
            {
                return Convert.ChangeType(text, type);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
