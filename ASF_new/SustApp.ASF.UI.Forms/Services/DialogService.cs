﻿using System;
using System.Threading.Tasks;
using Blazored.Toast.Services;

namespace SustApp.ASF.UI.Forms.Services
{
    public class DialogService : IDialogService, IDialogEventsService
    {
        public event EventHandler<DialogMessage> OnDialogMessage;

        public Task<bool> ConfirmAsync(string title, string message)
        {
            var taskCompletionSource = new TaskCompletionSource<bool>();
            
            // Task is set as completed when callback is called
            // Callback is called by the Dialog
            RaiseOnDialogMessage(new ConfirmDialogMessage(taskCompletionSource.SetResult, title, message));

            return taskCompletionSource.Task;
        }

        public Task ShowErrorAsync(string title, string message)
        {
            var taskCompletionSource = new TaskCompletionSource<bool>();

            // Task is set as completed when callback is called
            // Callback is called by the Dialog
            RaiseOnDialogMessage(new ErrorDialogMessage(() => taskCompletionSource.SetResult(true), title, message));

            return taskCompletionSource.Task;
        }

        public Task ToastAsync(string title, string message, ToastLevel toastLevel)
        {
            //var taskCompletionSource = new TaskCompletionSource<bool>();

            // Qua NON serve bloccare l'interfaccia però visto che la Toast, scompare da sola:
            //RaiseOnDialogMessage(new ToastDialogMessage(() => taskCompletionSource.SetResult(true), title, message, toastLevel));
            RaiseOnDialogMessage(new ToastDialogMessage(title, message, toastLevel));

            return Task.CompletedTask;

            //Questo è il modo x tornare un VOID come Task di un metodo NON asincrono???
            //return taskCompletionSource.Task;
        }

        private void RaiseOnDialogMessage(DialogMessage message)
        {
            OnDialogMessage?.Invoke(this, message);
        }
    }
}
