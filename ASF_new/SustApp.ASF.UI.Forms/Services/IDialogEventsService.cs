﻿using System;

namespace SustApp.ASF.UI.Forms.Services
{
    public interface IDialogEventsService
    {
        event EventHandler<DialogMessage> OnDialogMessage;
    }
}
