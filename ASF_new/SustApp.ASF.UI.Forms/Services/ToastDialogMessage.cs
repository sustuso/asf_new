﻿using System;
using Blazored.Toast.Services;

namespace SustApp.ASF.UI.Forms.Services
{
    public class ToastDialogMessage : DialogMessage
    {
        private readonly Action _callback;

        //public ToastDialogMessage(Action callback, string title, string message, ToastLevel toastLevel) : base(title, message, toastLevel)
        //{
        //    _callback = callback;
        //}

        public ToastDialogMessage(string title, string message, ToastLevel toastLevel) : base(title, message)
        {
            base.ToastLevel = toastLevel;
        }

        public void Complete()
        {
            _callback();
        }
    }
}
