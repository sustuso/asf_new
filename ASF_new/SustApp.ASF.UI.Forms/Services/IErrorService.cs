﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace SustApp.ASF.UI.Forms.Services
{
    public interface IErrorService
    {
        Task RunAsync(Func<CancellationToken, Task> task, CancellationToken token = default) => RunAsync(task, null, token);

        Task RunAsync(Func<CancellationToken, Task> task, SpinnerContext spinnerContext, CancellationToken token = default);
    }
}
