﻿using System;

namespace SustApp.ASF.UI.Forms.Services
{
    public class ConfirmDialogMessage : DialogMessage
    {
        private readonly Action<bool> _callback;

        public ConfirmDialogMessage(Action<bool> callback, string title, string message) : base(title, message)
        {
            _callback = callback;
        }

        public void Complete(bool result)
        {
            _callback(result);
        }
    }
}
