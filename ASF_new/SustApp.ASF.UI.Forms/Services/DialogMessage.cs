﻿using Blazored.Toast.Services;

namespace SustApp.ASF.UI.Forms.Services
{
    public class DialogMessage
    {
        public string Title { get; }

        public string Message { get; }

        public ToastLevel ToastLevel { get; set; }

        public DialogMessage(string title, string message)
        {
            Title = title;
            Message = message;
        }
    }
}
