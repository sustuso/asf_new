﻿using System.Threading.Tasks;
using Blazored.Toast.Services;

namespace SustApp.ASF.UI.Forms.Services
{
    public interface IDialogService
    {
        Task ShowErrorAsync(string title, string message);

        Task<bool> ConfirmAsync(string title, string message);

        Task ToastAsync(string title, string message, ToastLevel toastLevel);
    }
}
