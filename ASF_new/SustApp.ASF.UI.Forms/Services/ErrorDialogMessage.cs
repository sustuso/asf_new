﻿using System;

namespace SustApp.ASF.UI.Forms.Services
{
    public class ErrorDialogMessage : DialogMessage
    {
        private readonly Action _callback;

        public ErrorDialogMessage(Action callback, string title, string message) : base(title, message)
        {
            _callback = callback;
        }

        public void Complete()
        {
            _callback();
        }
    }
}
