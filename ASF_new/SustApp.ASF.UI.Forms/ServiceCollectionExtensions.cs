﻿using System;
using System.Collections.Generic;
using System.Text;
using Blazored.Modal;
using Blazored.Toast;
using SustApp.ASF.UI.Forms.Services;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddASFForms(this IServiceCollection services)
        {
            services.AddBlazoredModal();
            services.AddBlazoredToast();

            services.AddScoped<DialogService>();
            services.AddTransient<IDialogService>(p => p.GetRequiredService<DialogService>());
            services.AddTransient<IDialogEventsService>(p => p.GetRequiredService<DialogService>());

            return services;
        }
    }
}
