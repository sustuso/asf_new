﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Microsoft.AspNetCore.Components;

namespace SustApp.ASF.UI.Forms
{
    public class SustAppComponentBase : ComponentBase, IDisposable
    {
        private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

        protected CancellationToken Token => cancellationTokenSource.Token;

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                cancellationTokenSource.Cancel();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
