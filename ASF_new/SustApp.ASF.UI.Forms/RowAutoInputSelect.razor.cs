﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SustApp.ASF.DomainModel;
using SustApp.ASF.UI.Services;
using Microsoft.AspNetCore.Components;

namespace SustApp.ASF.UI.Forms
{
    public partial class RowAutoInputSelect<TEntity, TKey>
        where TEntity : class, IEntity<TKey>
    {
        private CountResult<TEntity> result;

        private TKey emptyValue = GetEmptyValue();
        private TKey internalValue;
        private Func<TEntity, string> displayFunc;

        [Parameter]
        public TKey Value { get; set; }

        [Parameter]
        public EventCallback<TKey> ValueChanged { get; set; }

        [Parameter]
        public Expression<Func<TKey>> ValueExpression { get; set; }

        [Parameter]
        public Expression<Func<TEntity, string>> DisplayExpression { get; set; }

        [Parameter]
        public EventCallback<TEntity> SelectedItemChanged { get; set; }

        [CascadingParameter]
        public SpinnerContext SpinnerContext { get; set; }

        [Parameter]
        public TEntity SelectedItem { get; set; }

        [Parameter]
        public bool IsOrdered { get; set; } = true;

        /// <summary>
        /// Lable added to html control
        /// </summary>
        [Parameter]
        public string Label { get; set; }

        /// <summary>
        /// Help added to html control
        /// </summary>
        [Parameter]
        public string Help { get; set; }

        [Parameter]
        public string EmptyItem { get; set; }

        [Parameter]
        public Action<RowAutoInputSelectEventArgs<TEntity, TKey>> OnLoadQueryItems { get; set; }

        public Task LoadItemsAsync() =>
            ErrorService.RunAsync(async t =>
            {
                // Load all items
                var request = new GetRequest<TEntity, TEntity>();
                request.QueryFactory = q =>
                {
                    // Order items using DisplayExpression
                    if (IsOrdered && DisplayExpression != null)
                    {
                        q = q.OrderBy(DisplayExpression);
                    }

                    if (OnLoadQueryItems != null)
                    {
                        var eventArgs = new RowAutoInputSelectEventArgs<TEntity, TKey>(q);
                        OnLoadQueryItems(eventArgs);
                        q = eventArgs.Query;
                    }

                    return q;
                };

                result = await ServiceAgent.GetAsync(request, t);
            }, SpinnerContext, Token);

        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();

            await LoadItemsAsync();
        }

        protected override async Task OnParametersSetAsync()
        {
            await base.OnParametersSetAsync();

            // Nothing to do
            // Result has not been loaded, probably due a load error
            // Also happens when you leave the page
            if (result == null) return;

            if (DisplayExpression != null)
            {
                // Convert expression into a real function called to display text into option
                displayFunc = DisplayExpression.Compile();
            }
            else
            {
                // Default implementation in case DisplayExpression is not set
                displayFunc = e => e.ToString();
            }

            // If the value is the default value
            if (EqualityComparer<TKey>.Default.Equals(Value, default))
            {
                // We use an internal value to represent an empty value (a random string)
                internalValue = emptyValue;
            }
            else
            {
                // Value and internvalValue matches
                internalValue = Value;
            }

            // Align SelectedItem to Value
            await SelectedItemAsync(Value);
        }

        private async Task OnValueChanged(TKey id)
        {
            internalValue = id;
            Value = id;

            // If the empty item has been selected
            if (EqualityComparer<TKey>.Default.Equals(emptyValue, id))
            {
                // Set the real value to default
                Value = default;
            }

            // Align SelectedItem to Value
            await SelectedItemAsync(Value);

            // Raise event
            await ValueChanged.InvokeAsync(Value);
        }

        private async Task SelectedItemAsync(TKey id)
        {
            if (SelectedItemChanged.HasDelegate)
            {
                // Look for item that matches the id
                var item = result.Items.FirstOrDefault(i => i.Id.Equals(id));
                // Raise event only if SelectedItem has changed to avoid loops
                if (!EqualityComparer<TEntity>.Default.Equals(SelectedItem, item))
                {
                    SelectedItem = item;
                    await SelectedItemChanged.InvokeAsync(SelectedItem);
                }
            }
        }

        private static TKey GetEmptyValue()
        {
            // Use a random string in case of string type
            if (typeof(TKey) == typeof(string))
            {
                return (TKey)(object)Guid.NewGuid().ToString("N");
            }
            // Use the default value type
            if (typeof(TKey).IsValueType)
            {
                return Activator.CreateInstance<TKey>();
            }

            throw new NotSupportedException();
        }
    }

    public class RowAutoInputSelectEventArgs<TEntity, TKey> : EventArgs
        where TEntity : class, IEntity<TKey>
    {
        public IQueryable<TEntity> Query { get; set; }

        public RowAutoInputSelectEventArgs(IQueryable<TEntity> query)
        {
            Query = query;
        }
    }
}
