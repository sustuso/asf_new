﻿using System;
using System.Net.Http.Headers;
using SustApp.ASF.DomainModel;
using SustApp.ASF.UI.ServiceAgent;
using SustApp.ASF.UI.Services;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServiceAgent(this IServiceCollection services)
        {
            //Constant operations:
            services.AddTransient<RestClient>();
            services.AddTransient(typeof(ODataClient<,>));

            //Variable operations:
            services.AddRestServiceAgent<Site, Guid>();
            services.AddRestServiceAgent<Exam, Guid>();
            services.AddRestServiceAgent<FormField, Guid>();
            services.AddRestServiceAgent<MeasureUnit, string>();
            services.AddRestServiceAgent<Measurement, string>();
            services.AddRestServiceAgent<MedicalSpecialization, string>();
            services.AddRestServiceAgent<FormFieldResult, Guid>();
            services.AddRestServiceAgent<ExamResult, Guid>();
            services.AddRestServiceAgent<MilitaryRank, string>();
            services.AddRestServiceAgent<City, string>();
            services.AddRestServiceAgent<Person, Guid>();

            return services;
        }

        private static IServiceCollection AddRestServiceAgent<TEntity, TKey>(this IServiceCollection services)
            where TEntity : class, IEntity<TKey>
        {
            return services.AddTransient<IServiceAgent<TEntity, TKey>, RestServiceAgentBase<TEntity, TKey>>();
        }
    }
}

namespace System.Net.Http
{
    public static class ClientExtensions
    {
        /// <summary>
        /// Set a BaseAddress property (e.g. https://localhost:44334/)
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public static HttpClient PrepareEIClient(this HttpClient client)
        {
            return PrepareEIClient(client, null);
        }

        /// <summary>
        /// Set a BaseAddress property (e.g. https://localhost:44334/)
        /// </summary>
        /// <param name="client"></param>
        /// <param name="accessTokenResult"></param>
        /// <returns></returns>
        public static HttpClient PrepareEIClient(this HttpClient client, AccessTokenResult accessTokenResult)
        {
#if DEBUG
            var baseUri = new Uri("https://localhost:44334/");
            if (client.BaseAddress != baseUri)
            {
                client.BaseAddress = baseUri;
            }
#endif
            if (accessTokenResult != null && accessTokenResult.TryGetToken(out AccessToken token))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.Value);
            }

            return client;
        }
    }
}
