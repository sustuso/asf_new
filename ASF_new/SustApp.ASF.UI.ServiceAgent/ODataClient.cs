﻿using SustApp.ASF.DomainModel;
using Microsoft.Extensions.Logging;
using Simple.OData.Client;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;

namespace SustApp.ASF.UI.ServiceAgent
{
    /// <summary>
    /// Class to inject in ASF.UI for call OData WS
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ODataClient<T, TKey>
        where T : class, IEntity<TKey>
    {
        private readonly HttpClient _client;
        private readonly IAccessTokenProvider _accessTokenProvider;

        public ODataClient(HttpClient client, IAccessTokenProvider accessTokenProvider)
        {
            _client = client;
            _accessTokenProvider = accessTokenProvider;
        }

        protected ODataClient(HttpClient client, ILogger logger, IAccessTokenProvider accessTokenProvider)
        {
            _client = client;
            _accessTokenProvider = accessTokenProvider;
        }

        #region CRUD Operations

        /// <summary>
        /// Get data from OData WS
        /// </summary>
        public IBoundClient<T> Entities => GetClient().For<T>();

        public IODataClient Client => GetClient();

        /// <summary>
        /// Insert Entity using OData WS
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<T> InsertAsync(T entity, CancellationToken token)
        {
            var boundClient = GetClient().For<T>();
            if (entity.GetType() == typeof(T))
            {
                T result = await boundClient
                    .Set(entity)
                    .InsertEntryAsync(token);
                return result;
            }
            else //Derived class (e.g. Evaluetion)
            {
                return await boundClient
                    // TODO: typed
                    //.As<TEntity>()
                    .Set(entity)
                    .InsertEntryAsync(token);
            }
        }

        /// <summary>
        /// Updata Entity using OData WS
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<T> UpdateAsync(T entity, CancellationToken token)
        {
            var boundClient = GetClient(ODataUpdateMethod.Put).For<T>();

            if (entity.GetType() == typeof(T))
            {
                var result = await boundClient
                    .Key(entity.Id)
                    .Set(entity)
                    .UpdateEntryAsync(token);

                return result;
            }
            else //Derived class
            {
                return await boundClient
                    // TODO: typed
                    //.As<TEntity>()
                    .Key(entity.Id)
                    .Set(entity)
                    .UpdateEntryAsync(token);
            }
        }

        /// <summary>
        /// Patch (update only changed records) Entity using OData WS
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<T> PatchAsync(T entity, CancellationToken token)
        {
            var boundClient = GetClient().For<T>();
            if (entity.GetType() == typeof(T))
            {
                T result = await boundClient
                    .Key(entity.Id)
                    .Set(entity)
                    .UpdateEntryAsync(token);
                return result;
            }
            else //Derived class
            {
                return await boundClient
                        // TODO: typed
                    //.As<TEntity>()
                    .Key(entity.Id)
                    .Set(entity)
                    .UpdateEntryAsync(token);
            }
        }

        /// <summary>
        /// Delete Entity using OData WS
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Task DeleteAsync(T entity, CancellationToken token)
        {
            var boundClient = GetClient().For<T>();
            if (entity.GetType() == typeof(T))
            {
                return boundClient
                    .Key(entity.Id)
                    .DeleteEntryAsync(token);
            }
            else //Derived class
            {
                return boundClient
                    .As(entity.GetType().Name)
                    .Key(entity.Id)
                    .DeleteEntryAsync(token);
            }
        }
        #endregion

        /// <summary>
        /// Return ODataClient for Get operation
        /// </summary>
        /// <param name="preferredUpdateMethod"></param>
        /// <returns></returns>
        protected ODataClient GetClient(ODataUpdateMethod preferredUpdateMethod = ODataUpdateMethod.Patch)
        {
            _client.PrepareEIClient();

            var settings = new ODataClientSettings(_client, new Uri("/odata", UriKind.Relative))
            {
                PreferredUpdateMethod = preferredUpdateMethod,
                IncludeAnnotationsInResults = true,
                IgnoreUnmappedProperties = true,
                BeforeRequestAsync = OnBeforeRequest,
                // Non necessario perché HttpClient ha già il suo log
                // OnTrace = (s, p) => _logger.LogDebug(String.Format(s, p)) //Cosa fa?
            };
            var client = new ODataClient(settings);

            return client;
        }

        private async Task OnBeforeRequest(HttpRequestMessage arg)
        {
            AccessTokenResult accessTokenResult = await _accessTokenProvider.RequestAccessToken();

            if (accessTokenResult.TryGetToken(out AccessToken token))
            {
                arg.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token.Value);
            }
        }
    }   
}