﻿using SustApp.ASF.DomainModel;
using SustApp.ASF.UI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace SustApp.ASF.UI.ServiceAgent
{
    public class RestServiceAgentBase<T, TKey> : IServiceAgent<T, TKey>
        where T : class, IEntity<TKey>
    {
        protected RestClient Client { get; }

        public Uri BaseUri { get; }

        public RestServiceAgentBase(RestClient client) : this(client, new Uri($"/api/{typeof(T).Name}", UriKind.Relative))
        {
        }

        public RestServiceAgentBase(RestClient client, Uri baseUri)
        {
            Client = client;
            BaseUri = baseUri;
        }

        #region CRUD Operations

        /// <summary>
        /// Add T entity domain class
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public virtual async Task<T> AddAsync(T entity, CancellationToken token)
        {
            try
            {
                return await Client.PostAsync(BaseUri, entity, token);
            }
            catch (WebException we)
            {
                throw new SustAppEntityException(entity, "Error while adding entity", we);
            }
        }

        /// <summary>
        /// Update T entity domain class 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public virtual async Task<T> UpdateAsync(T entity, CancellationToken token)
        {
            try
            {
                return await Client.PutAsync(BaseUri, entity, token);
            }
            catch (WebException we)
            {
                // TODO: versione
                //if (we.Status == (WebExceptionStatus)409)
                throw new SustAppEntityException(entity, "Error while updating entity", we);
            }
        }

        /// <summary>
        /// Delete T entity domain class 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public virtual Task DeleteAsync(T entity, CancellationToken token)
        {
            try
            {
                return Client.DeleteAsync(BaseUri, entity, token);
            }
            catch (WebException we)
            {
                // TODO: versione
                //if (we.Status == (WebExceptionStatus)409)
                throw new SustAppEntityException(entity, "Error while deleting entity", we);
            }
        }

        /// <summary>
        /// Get a single entity domain class by PK 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public virtual Task<T> GetAsync(TKey id, CancellationToken token)
        {
            try
            {
                return Client.GetAsync<T>(new Uri($"{BaseUri}/{id}", UriKind.Relative), token);
            }
            catch (WebException we)
            {
                // TODO: versione
                //if (we.Status == (WebExceptionStatus)409)
                throw new SustAppEntityException(id, "Error while Get entity", we);
            }
        }

        /// <summary>
        /// Get a IReadOnlyList of entity domain class quering by razor component
        /// Es. await XXAgent.GetAsync(q => q.Take(5).Select(r => new { r.Id }), CancellationToken.None);
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        public virtual async Task<CountResult<TResult>> GetAsync<TResult>(GetRequest<T, TResult> request, CancellationToken token) where TResult : class
        {
            try
            {
                if (request.WithCount)
                {
                    return await Client.GetAsync<T, CountResult<TResult>, TResult>(BaseUri, request, token);
                }
                else
                {
                    var items = await Client.GetAsync<T, IReadOnlyList<TResult>, TResult>(BaseUri, request, token);
                    token.ThrowIfCancellationRequested();

                    return new CountResult<TResult>(items, 0);
                }
            }
            catch (WebException we)
            {
                // TODO: versione
                //if (we.Status == (WebExceptionStatus)409)
                throw new SustAppEntityException("?? Cosa passiamo??", "Error while deleting entity", we);
            }
        }

        #endregion
    }
}