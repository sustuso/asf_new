﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using SustApp.ASF.DomainModel.Json;
using SustApp.ASF.UI.Services;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;

namespace SustApp.ASF.UI.ServiceAgent
{
    public class RestClient
    {
        private readonly HttpClient _client;

        private readonly IAccessTokenProvider _accessTokenProvider;

        private static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Objects,
            SerializationBinder = new SimpleSerializationBinder(),
            ContractResolver = new DeepContractResolver()
        };

        public RestClient(HttpClient client, IAccessTokenProvider accessTokenProvider)
        {
            _client = client;
            _accessTokenProvider = accessTokenProvider;
        }

        #region CRUD Operations        

        /// <summary>
        /// Get a IReadOnlyList of entity domain class quering by razor component
        /// Es. await XXAgent.GetAsync(uri, q => q.Take(5).Select(r => new { r.Id }), CancellationToken.None);
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="uri"></param>
        /// <param name="request"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns>IReadOnlyList</returns>
        public Task<TList> GetAsync<T, TList, TResult>(Uri uri, GetRequest<T, TResult> request, CancellationToken token = default)
        {
            //async/await ????????

            //Retry the correct query string filter:
            string queryString = UriFactory.GetRelativeUriForQuery(request.QueryFactory, request.ExpandExpressions);

            // Count required
            if (request.WithCount)
            {
                queryString += "&$count=true";
            }

            return GetAsync<TList>(new Uri(uri + queryString, UriKind.Relative), token);

        }

        /// <summary>
        /// Execute a GET call to REST WS
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<T> GetAsync<T>(Uri uri, CancellationToken token = default)
        {
            AccessTokenResult accessTokenResult = await _accessTokenProvider.RequestAccessToken();
            token.ThrowIfCancellationRequested();

            using (var response = await _client.PrepareEIClient(accessTokenResult).GetAsync(uri, token))
            {
                response.EnsureSuccessStatusCode();
                var bodyResponse = await response.Content.ReadAsStringAsync();
                token.ThrowIfCancellationRequested();

                return JsonConvert.DeserializeObject<T>(bodyResponse, Settings);
            }
        }

        /// <summary>
        /// Execute a Post call to REST WS (Add)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="entity"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public async Task<T> PostAsync<T>(Uri uri, T entity, CancellationToken token = default)
        {
            AccessTokenResult accessTokenResult = await _accessTokenProvider.RequestAccessToken();

            using (var response = await _client.PrepareEIClient(accessTokenResult).PostAsync(uri, new JsonContent(entity), token))
            {
                var bodyResponse = await response.Content.ReadAsStringAsync();
                token.ThrowIfCancellationRequested();

                response.EnsureSuccessStatusCode();

                return JsonConvert.DeserializeObject<T>(bodyResponse, Settings);
            }
        }

        /// <summary>
        /// Execute a Put call to REST WS (Update)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="entity"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public async Task<T> PutAsync<T>(Uri uri, T entity, CancellationToken token = default)
        {
            AccessTokenResult accessTokenResult = await _accessTokenProvider.RequestAccessToken();

            using (var response = await _client.PrepareEIClient(accessTokenResult).PutAsync(uri, new JsonContent(entity), token))
            {
                response.EnsureSuccessStatusCode();
                var bodyResponse = await response.Content.ReadAsStringAsync();
                token.ThrowIfCancellationRequested();

                return JsonConvert.DeserializeObject<T>(bodyResponse, Settings);
            }
        }

        /// <summary>
        /// Execute a Delete call to REST WS
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public async Task DeleteAsync<T>(Uri uri, CancellationToken token = default)
        {
            AccessTokenResult accessTokenResult = await _accessTokenProvider.RequestAccessToken();

            using (var response = await _client.PrepareEIClient(accessTokenResult).DeleteAsync(uri, token))
            {
                response.EnsureSuccessStatusCode();
                var bodyResponse = await response.Content.ReadAsStringAsync();
                token.ThrowIfCancellationRequested();
            }

        }

        /// <summary>
        /// Execute a Delete call to REST WS by Entity as parameter
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="entity">Entity to delete</param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public async Task DeleteAsync<T>(Uri uri, T entity, CancellationToken token = default)
        {
            AccessTokenResult accessTokenResult = await _accessTokenProvider.RequestAccessToken();

            var request = new HttpRequestMessage(HttpMethod.Delete, uri) { Content = new JsonContent(entity) };

            using (var response = await _client.PrepareEIClient(accessTokenResult).SendAsync(request, token))
            {
                response.EnsureSuccessStatusCode();
                var bodyResponse = await response.Content.ReadAsStringAsync();
                token.ThrowIfCancellationRequested();
            }

        }

        #endregion
    }

    public class JsonContent : StringContent
    {
        public JsonContent(object obj) :
            base(JsonConvert.SerializeObject(obj), System.Text.Encoding.UTF8, "application/json")
        { }
    }
}