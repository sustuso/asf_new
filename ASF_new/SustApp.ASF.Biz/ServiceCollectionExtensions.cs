﻿using System;
using System.Collections.Generic;
using System.Text;
using SustApp.ASF.Biz;
using SustApp.ASF.DomainModel;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddBiz(this IServiceCollection services)
        {
            services.AddAuditableBiz<Site, Guid>();
            services.AddAuditableBiz<FormField, Guid>();
            services.AddAuditableBiz<Exam, Guid>();
            services.AddAuditableBiz<Measurement, string>();
            services.AddAuditableBiz<MeasureUnit, string>();
            services.AddAuditableBiz<MedicalSpecialization, string>();
            services.AddAuditableBiz<FormFieldResult, Guid>();
            services.AddAuditableBiz<ExamResult, Guid>();
            services.AddAuditableBiz<MilitaryRank, string>();
            services.AddAuditableBiz<City, string>();
            services.AddAuditableBiz<Person, Guid>();

            return services;
        }

        /// <summary>
        /// Register AddTransient DI for IBiz<T> base class with a CRUD operations methods
        /// </summary>
        /// <typeparam name="TBiz"></typeparam>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="services"></param>
        /// <returns></returns>
        private static IServiceCollection AddBiz<TBiz, TEntity, TKey>(this IServiceCollection services)
            where TBiz : class, IBiz<TEntity, TKey>
            where TEntity : IEntity<TKey>
        {
            services.AddTransient<TBiz>();
            services.AddTransient<IBiz<TEntity, TKey>, TBiz>();
            return services;
        }

        /// <summary>
        /// Register AddTransient DI for IBiz<T> base class and TIBiz derived class with a custom operations methods
        /// </summary>
        /// <typeparam name="TBiz">e.g. SiteBiz</typeparam>
        /// <typeparam name="TIBiz">e.g. ISiteBiz</typeparam>
        /// <typeparam name="TEntity">e.g. Site</typeparam>
        /// <param name="services"></param>
        /// <returns></returns>
        private static IServiceCollection AddBiz<TBiz, TIBiz, TEntity, TKey>(this IServiceCollection services)
            where TBiz : class, IBiz<TEntity, TKey>, TIBiz
            where TIBiz : class, IBiz<TEntity, TKey>
            where TEntity : IEntity<TKey>
        {
            services.AddTransient<TBiz>();
            services.AddTransient<IBiz<TEntity, TKey>, TBiz>();
            services.AddTransient<TIBiz, TBiz>();
            return services;
        }

        /// <summary>
        /// Register AddTransient DI for BaseBiz class and IBiz interface
        /// </summary>
        /// <typeparam name="T">IEntity (e.g. Site)</typeparam>
        /// <param name="services"></param>
        /// <returns></returns>
        private static IServiceCollection AddBaseBiz<T, TKey>(this IServiceCollection services)
            where T : IEntity<TKey>
        {
            services.AddTransient<BaseBiz<T, TKey>>();
            services.AddTransient<IBiz<T, TKey>, BaseBiz<T, TKey>>();
            return services;
        }

        /// <summary>
        /// Register AddTransient DI for AuditableBiz class and IBiz interface
        /// </summary>
        /// <typeparam name="T">IEntity (e.g. Phase)</typeparam>
        /// <param name="services"></param>
        /// <returns></returns>
        private static IServiceCollection AddAuditableBiz<T, TKey>(this IServiceCollection services)
            where T : IEntity<TKey>
        {
            services.AddTransient<AuditableBiz<T, TKey>>();
            services.AddTransient<IBiz<T, TKey>, AuditableBiz<T, TKey>>();
            return services;
        }
    }
}
