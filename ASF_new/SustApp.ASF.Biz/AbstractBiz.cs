﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Security;
using System.Threading.Tasks;
using SustApp.ASF.DomainModel;
using Microsoft.EntityFrameworkCore;

namespace SustApp.ASF.Biz
{
    public abstract class AbstractBiz<T, TKey> : IBiz<T, TKey>
        where T : IEntity<TKey>
    {
        #region IQueryable implementations

        IEnumerator<T> IEnumerable<T>.GetEnumerator() => Query.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => Query.GetEnumerator();

        Type IQueryable.ElementType => Query.ElementType;

        Expression IQueryable.Expression => Query.Expression;

        IQueryProvider IQueryable.Provider => Query.Provider;

        protected abstract IQueryable<T> Query { get; }

        #endregion

        #region Abstarct Methods for BaseBiz override

        protected abstract Task AddInternalAsync(T entity);

        protected abstract Task UpdateInternalAsync(T entity);        

        protected abstract Task DeleteInternalAsync(T entity);

        protected abstract Task DeleteInternalAsync(TKey id, byte[] version);

        #endregion

        #region CRUD Operation used from controllers
        
        /// <summary>
        /// Add T entity in database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task AddAsync(T entity)
        {
            try
            {
                await AddInternalAsync(entity);
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException(entity, $"Error while adding entity {entity.Id}", e);
            }
        }

        /// <summary>
        /// Update T entity in database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task UpdateAsync(T entity)
        {
            try
            {
                await UpdateInternalAsync(entity);
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException(entity, $"Error while updating entity with id {entity.Id}", e);
            }
        }

        /// <summary>
        /// Delete an entity by PK without RowVersion for optimistic concurrency
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteAsync(TKey id)
        {
            try
            {
                await DeleteInternalAsync(id);
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException(id, $"Error while deleting entity with id {id}", e);
            }
        }

        /// <summary>
        /// Delete an entity by PK with RowVersion for optimistic concurrency
        /// </summary>
        /// <param name="id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public async Task DeleteAsync(TKey id, byte[] version)
        {
            try
            {
                await DeleteInternalAsync(id, version);
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException(id, $"Error while deleting entity with id {id}", e);
            }
        }

        /// <summary>
        /// Delete an entity without RowVersion
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task DeleteAsync(T entity)
        {
            try
            {
                await DeleteInternalAsync(entity);
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException(entity, $"Error while deleting entity with id {entity.Id}", e);
            }
        }

        /// <summary>
        /// Get a single enity by PK
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<T> GetAsync(TKey id)
        {
            try
            {
                return await GetInternalAsync(id);
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException(id, $"Error while reading entity with id {id}", e);
            }
        }

        #endregion

        #region Virtual Methods        

        protected virtual Task DeleteInternalAsync(TKey id) => DeleteInternalAsync(id, null);

        protected virtual Task<T> GetInternalAsync(TKey id)
        {
            return Query.FirstOrDefaultAsync(i => i.Id.Equals(id));
        }

        /// <summary>
        /// Try to validate the param object
        /// </summary>
        /// <param name="entity">T</param>
        /// <returns></returns>
        protected virtual Task ValidateEntityAsync(T entity)
        {
            var result = new List<ValidationResult>();
            if (!Validator.TryValidateObject(entity, new ValidationContext(entity), result, true))
            {
                throw new SustAppValidationException(entity, result);
            }

            return Task.CompletedTask;
        }

        protected virtual async Task CheckAccessAsync(TKey id)
        {
            if (!await Query.AnyAsync(i => i.Id.Equals(id)))
            {
                throw new SecurityException($"Cannot access to entity with id {id} of type {typeof(T).Name}");
            }
        }

        #endregion
    }
}