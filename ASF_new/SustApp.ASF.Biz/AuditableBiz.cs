﻿using SustApp.ASF.Dal;
using SustApp.ASF.DomainModel;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Transactions;

namespace SustApp.ASF.Biz
{
    public class AuditableBiz<T, TKey> : BaseBiz<T, TKey>
        where T : IEntity<TKey>
    {
        private readonly IDataLayer<Audit, Guid> _auditDataLayer;
        private readonly ClaimsPrincipal _principal;

        public AuditableBiz(IDataLayer<Audit, Guid> auditDataLayer, IDataLayer<T, TKey> dataLayer, ClaimsPrincipal principal) : base(dataLayer)
        {
            _auditDataLayer = auditDataLayer ?? throw new ArgumentNullException(nameof(auditDataLayer));
            _principal = principal;
        }

        /// <summary>
        /// Add internal T entity in database
        /// No Try/Catch because present in AbstractBiz
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        protected override async Task AddInternalAsync(T entity)
        {
            using var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            await base.AddInternalAsync(entity);

            await _auditDataLayer.AddAsync(new Audit(_principal.Identity.Name, "Add", entity));

            scope.Complete();
        }

        /// <summary>
        /// Update internal T entity in database
        /// No Try/Catch because present in AbstractBiz
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        protected override async Task UpdateInternalAsync(T entity)
        {
            using var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            await base.UpdateInternalAsync(entity);

            await _auditDataLayer.AddAsync(new Audit(_principal.Identity.Name, "Update", entity));

            scope.Complete();
        }

        /// <summary>
        /// Delete internal an entity by PK with RowVersion for optimistic concurrency
        /// No Try/Catch because present in AbstractBiz
        /// </summary>
        /// <param name="id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        protected override async Task DeleteInternalAsync(TKey id, byte[] version)
        {
            using var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            await base.DeleteInternalAsync(id, version);

            await _auditDataLayer.AddAsync(new Audit(_principal.Identity.Name, "Delete", typeof(T).Name, id.ToString()));

            scope.Complete();
        }

        /// <summary>
        /// Get internal a single enity by PK
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected override Task<T> GetInternalAsync(TKey id)
        {
            return GetAsync(id, true);
        }

        /// <summary>
        /// Get Async by ID and audit
        /// </summary>
        /// <param name="id"></param>
        /// <param name="audit"></param>
        /// <returns></returns>
        public virtual async Task<T> GetAsync(TKey id, bool audit)
        {
            try
            {
                T entity = await base.GetInternalAsync(id);

                if (audit)
                {
                    await _auditDataLayer.AddAsync(new Audit(_principal.Identity.Name, "Read", typeof(T).Name, id.ToString()));
                }

                return entity;
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException(id, $"Error while reading entity with id {id}", e);
            }
        }
    }
}
