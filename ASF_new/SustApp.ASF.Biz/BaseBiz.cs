﻿using SustApp.ASF.Dal;
using SustApp.ASF.DomainModel;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SustApp.ASF.Biz
{
    public class BaseBiz<T, TKey> : AbstractBiz<T, TKey>
        where T : IEntity<TKey>
    {
        private readonly IDataLayer<T, TKey> _dataLayer;

        public BaseBiz(IDataLayer<T, TKey> dataLayer)
        {
            _dataLayer = dataLayer ?? throw new ArgumentNullException(nameof(dataLayer));
        }


        #region Override Methods
        
        protected override IQueryable<T> Query => _dataLayer;

        protected override async Task AddInternalAsync(T entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));

            await ValidateEntityAsync(entity);

            await _dataLayer.AddAsync(entity);
        }

        protected override async Task UpdateInternalAsync(T entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));

            await CheckAccessAsync(entity.Id);

            await ValidateEntityAsync(entity);

            await _dataLayer.UpdateAsync(entity);
        }

        protected override Task DeleteInternalAsync(TKey id)
        {
            // If T implements IVersionedEntity we cannot delete without the version specified
            if (typeof(IVersionedEntity<TKey>).IsAssignableFrom(typeof(T)))
            {
                throw new InvalidOperationException($"Cannot delete entity {id} of type {typeof(T).Name} without a version");
            }

            return DeleteInternalAsync(id, null);
        }

        protected override async Task DeleteInternalAsync(TKey id, byte[] version)
        {
            await CheckAccessAsync(id);

            await _dataLayer.DeleteAsync(id, version);
        }

        protected override async Task DeleteInternalAsync(T entity)
        {
            await CheckAccessAsync(entity.Id);

            await _dataLayer.DeleteAsync(entity);
        }

        #endregion
    }
}
