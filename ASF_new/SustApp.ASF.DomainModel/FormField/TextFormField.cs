﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SustApp.ASF.DomainModel
{
    public class TextFormField : FormField
    {
        [Range(1, 1_000_000)]
        public int Size { get; set; }

        public bool IsMultiline { get; set; }

        [StringLength(500)]
        public string ValidationRegexErrorMessage { get; set; }

        [StringLength(500)]
        public string ValidationRegex { get; set; }
    }
}
