﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SustApp.ASF.DomainModel
{
    public class DateFormField : FormField
    {
        public DateFormFieldType Type { get; set; }
    }

    public enum DateFormFieldType
    {
        Date,
        Time,
        DateTime
    }
}
