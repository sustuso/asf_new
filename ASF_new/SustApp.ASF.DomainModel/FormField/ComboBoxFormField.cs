﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SustApp.ASF.DomainModel
{
    public class ComboBoxFormField : FormField
    {
        public List<string> Items { get; private set; } = new List<string>();
    }
}
