﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SustApp.ASF.DomainModel
{
    public class NumberFormField : FormField
    {
        public int Decimals { get; set; }

        public double MinimumValue { get; set; }

        public double MaximumValue { get; set; }
    }
}
