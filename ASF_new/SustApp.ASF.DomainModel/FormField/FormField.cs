﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SustApp.ASF.DomainModel
{
    public abstract class FormField : VersionedEntityBase<Guid>
    {
        [Required]
        [StringLength(200)]
        public string Label { get; set; }

        [Required]
        [StringLength(500)]
        public string Placeholder { get; set; }

        [Required]
        public string HelpText { get; set; }

        public bool IsMandatory { get; set; }

        public Guid? ParentFormFieldId { get; set; }

        public FormField ParentFormField { get; set; }

        public Exam Exam { get; set; }

        public Guid ExamId { get; set; }
    }

    //public class FormField : EntityBase<Guid>
    //{
    //    [Required]
    //    [StringLength(200)]
    //    public string Label { get; set; }

    //    [Required]
    //    [StringLength(500)]
    //    public string Placeholder { get; set; }

    //    [Required]
    //    public string HelpText { get; set; }

    //    public bool IsMandatory { get; set; }

    //    [StringLength(500)]
    //    public string ValidationRegexErrorMessage { get; set; }

    //    [StringLength(500)]
    //    public string ValidationRegex { get; set; }

    //    public int Size { get; set; }

    //    public List<string> Items { get; } = new List<string>();

    //    public Guid? ParentFormFieldId { get; set; }
    //}

    //public enum FormFieldType
    //{
    //    Text,
    //    Number,
    //    YesNo,
    //    Flag,
    //    Date,
    //    DateTime,
    //    Time,
    //    ComboBox,
    //    CheckBoxList
    //}
}
