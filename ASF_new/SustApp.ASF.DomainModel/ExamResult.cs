﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SustApp.ASF.DomainModel
{
    public class ExamResult : VersionedEntityBase<Guid>
    {
        public DateTimeOffset Date { get; set; }

        public Exam Exam { get; set; }

        public Guid ExamId { get; set; }

        public List<FormFieldResult> FormFields { get; } = new List<FormFieldResult>();
    }

}
