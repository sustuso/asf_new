﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SustApp.ASF.DomainModel
{
    public class BaseEmailPassword
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Email cannot be longer than 50 and smaller than 2 characters.")]
        string Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Password cannot be longer than 50 and smaller than 2 characters.")]
        string Password { get; set; }
    }
}
