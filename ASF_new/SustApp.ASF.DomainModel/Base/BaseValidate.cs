﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ASF.Model.Base
{
    public class BaseValidate
    {
        [Required]
        bool Enable { get; set; }
        [Required]
        bool Deleted { get; set; }

    }
}
