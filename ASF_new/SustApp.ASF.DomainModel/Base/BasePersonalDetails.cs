﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SustApp.ASF.DomainModel
{
    #region Interfaces
    public class BasePersonalDetail
    {
        [Required]
        [DataType(DataType.Text)]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Name cannot be longer than 50 and smaller than 2 characters.")]
        string Name { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Surname cannot be longer than 50 and smaller than 2 characters.")]
        string Surname { get; set; }
        [DataType(DataType.Date)]
        DateTime? DateOfBirth { get; set; }
        [DataType(DataType.Text)]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "PlaceOfBirth cannot be longer than 50 and smaller than 2 characters.")]
        Guid? GuidPlaceOfBirth { get; set; }
        Guid? GuidCountry { get; set; }
        [Required]
        Guid GuidLanguage { get; set; }
    }

    public class ContactList
    {
        List<Contacts> Contacts { get; set; }
    }

    public class AddressList
    {
        List<Addresses> Addresses { get; set; } 
    }

    #endregion

    #region Classes
    public class Contacts : EntityBase<Guid>
    {
        [Key]
        public override Guid Id  { get; set; }
        [Required]
        public Guid IdPerson { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [StringLength(50, MinimumLength = 2,  ErrorMessage = "Contact cannot be longer than 50 and smaller than 2 characters.")]
        public string Name { get; set; }
        [Required]
        public BaseEnumerators.ContacType ContactType { get; set; }
        [Required]
        public bool Default { get; set; }

    }

    public class Addresses : EntityBase<Guid>
    { 
        [Key]
        public override Guid Id  { get; set; }
        [Required]
        public Guid IdPerson { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Street cannot be longer than 50 and smaller than 2 characters.")]
        public string Street { get; set; }
        [Required]
        public City City { get; set; }
        [Required]
        public BaseEnumerators.AddressType AddressType { get; set; }
        [Required]
        public bool Default{ get; set; }
    }
    /*
    public class Cities : EntityBase<Guid>
    {
        [Key]
        public Guid CityGuid { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "City cannot be longer than 50 and smaller than 2 characters.")]
        public string Name { get; set; }
    }
    public class PostCodes
    {
        [Key]
        public Guid PostCodeGuid { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [StringLength(5, MinimumLength = 5, ErrorMessage = "PostCode cannot be longer than 50 and smaller than 2 characters.")]
        public string Code { get; set; }
    }
    */
    #endregion

}

