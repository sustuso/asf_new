﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SustApp.ASF.DomainModel
{
    public class BaseEnumerators
    {
        public enum ContacType
        {
            Void = 0,
            Telephone = 1,
            Mobile = 2,
            Fax = 3,
            Email = 4,
            Other = 5,
        }

        public enum AddressType
        {
            Void = 0,
            Home = 1,
            Work = 2,
            Branch = 3,
            Warehose = 4,
            Depot = 5,
            Other = 6,
        }
        
        public enum LevelType
        {
            Void = 0,
            Normal = 1,
            Privileged = 2,
            Administrator = 99,
            SuperAdministrator = 100,
        }
    }
}
