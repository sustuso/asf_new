﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SustApp.ASF.DomainModel
{
    public class BaseLevel
    {
        [Required]
        BaseEnumerators.LevelType LevelType { get; set; }
    }
}
