﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SustApp.ASF.DomainModel
{
    public class BaseTimed
    {
        [Required]
        [DataType(DataType.Date)]
        DateTime DateInsert { get; set; }
        [DataType(DataType.Date)]
        DateTime? DateDel { get; set; }
        [DataType(DataType.Date)]
        DateTime? DateModify { get; set; }
        Guid? UserInsert { get; set; }
        Guid? UserDel { get; set; }
        Guid? UserModify { get; set; }
    }
}
