﻿using System;
using System.Collections.Generic;
using System.Text;
using SustApp.ASF.DomainModel;

namespace SustApp.ASF.DomainModel
{
    public class SustAppVersionException : SustAppEntityException
    {
        public SustAppVersionException(IEntity entity) : this(entity, null)
        {
        }

        public SustAppVersionException(IEntity entity, Exception innerException) : base (entity, "Version specified is not valid", innerException)
        {
        }
    }
}
