﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SustApp.ASF.DomainModel
{
    public class City: EntityBase<string>
    {
        public override string Id { get; set; } //codice istat
        public string Comune { get; set; }
        public string Provincia { get; set; }
        public string Regione { get; set; }
        public string Prefisso { get; set; }
        public string CAP { get; set; }
        public string CodFisco { get; set; }
        public string Abitanti { get; set; }
        public string Link { get; set; }
    }
}
