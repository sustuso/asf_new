﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SustApp.ASF.DomainModel;
using Newtonsoft.Json.Serialization;

namespace SustApp.ASF.DomainModel.Json
{
    public class SimpleSerializationBinder : ISerializationBinder
    {
        public Type BindToType(string assemblyName, string typeName)
        {
            return Type.GetType($"{typeof(IEntity).Namespace}.{typeName}, {typeof(IEntity).Assembly.FullName}", true);
        }

        public void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            // TODO: find a way to exclude $type serialization
            //if (serializedType.IsGenericType && serializedType.GetGenericTypeDefinition() == typeof(PageResult<>))
            //{
            //    assemblyName = null;
            //    typeName = null;
            //    return;
            //}

            assemblyName = null;
            typeName = serializedType.Name;
        }
    }
}
