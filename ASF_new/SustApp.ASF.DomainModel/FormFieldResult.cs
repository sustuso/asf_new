﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SustApp.ASF.DomainModel
{
    public class FormFieldResult : VersionedEntityBase<Guid>
    {
        public string TextValue { get; set; }

        public double? NumberValue { get; set; }

        public bool? FlagValue { get; set; }

        public DateTime? DateValue { get; set; }

        public Guid FormFieldId { get; set; }

        public Guid ExamResultId { get; set; }

        public FormField FormField { get; set; }

        public ExamResult ExamResult { get; set; }
    }
}
