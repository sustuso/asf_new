﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace SustApp.ASF.DomainModel
{

    public class Person : EntityBase<Guid>
    //IBaseTimed, IBaseValidate, IBasePersonalDetail, IContactList, IAddressList
    {
        [Key]
        public override Guid Id { get; set; }
        [Required]
        public Guid IdASFCompany { get; set; }
        [StringLength(10, MinimumLength = 3, ErrorMessage = "Code cannot be longer than 10 and smaller than 3 characters.")]
        public string Code { get; set; }
        /*
        [Required]
        public PersonEnumerators.PersonType PersonType { get; set; }
        */
        [Required]
        public Guid IdCompany { get; set; }

        #region BaseTimed
        public DateTime DateInsert { get; set; }
        public DateTime? DateDel { get; set; }
        public DateTime? DateModify { get; set; }
        public Guid? UserInsert { get; set; }
        public Guid? UserDel { get; set; }
        public Guid? UserModify { get; set; }

        #endregion

        #region BaseValidate
        public bool Enable { get; set; }
        public bool Deleted { get; set; }
        #endregion

        #region BaseNamed
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public Guid? IdPlaceOfBirth { get; set; }
        public Guid? IdCountry { get; set; }
        public Guid IdLanguage { get; set; }

        #endregion

        #region ContactList
        
        public List<Contacts> Contacts { get; set; }
        #endregion

        #region AddressList
        public List<Addresses> Addresses { get; set; }
        #endregion

    }
}
