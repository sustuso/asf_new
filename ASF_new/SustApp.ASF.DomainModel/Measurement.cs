﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SustApp.ASF.DomainModel
{
    public class Measurement : EntityBase<string>
    {

        [StringLength(50)]
        public override string Id { get; set; }

        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [Required]
        public string MeasureUnitId { get; set; }

        public MeasureUnit MeasureUnit { get; set; }
    }
}
