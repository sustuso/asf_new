﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SustApp.ASF.DomainModel
{
    public class Audit : EntityBase<Guid>
    {
        public Audit(string user, string message, IEntity entity) : this(user, message, entity.GetType().Name, entity.ObjectId.ToString())
        {

        }

        public Audit(string user, string message, string entityName, string entityId)
        {
            User = user;
            Message = message;
            EntityName = entityName;
            EntityId = entityId;
            AuditDate = DateTimeOffset.Now;
        }

        public DateTimeOffset AuditDate { get; private set; }

        [Required]
        [StringLength(100)]
        public string User { get; }

        [Required]
        public string Message { get; }

        [Required]
        [StringLength(100)]
        public string EntityName { get; }

        public string EntityId { get; }
    }
}
