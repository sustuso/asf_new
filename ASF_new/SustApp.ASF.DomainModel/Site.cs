﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SustApp.ASF.DomainModel
{
    public class Site : EntityBase<Guid>
    {
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

    }
}
