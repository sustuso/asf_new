﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SustApp.ASF.DomainModel
{
    public class Exam : VersionedEntityBase<Guid>
    {
        [StringLength(200)]
        [Required]
        public string Description { get; set; }

        [Required]
        public string MeasurementId { get; set; }

        [Required]
        public string MedicalSpecializationId { get; set; }

        public MedicalSpecialization MedicalSpecialization { get; set; }

        public Measurement Measurement { get; set; }

        public List<FormField> FormFields { get; } = new List<FormField>();
    }

}
