﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SustApp.ASF.Biz;
using SustApp.ASF.DomainModel;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace SustApp.ASF.ConsoleApp
{
    class Test : IHostedService
    {
        private readonly IBiz<Site, Guid> _siteBiz;

        public Test(IBiz<Site, Guid> siteBiz)
        {
            _siteBiz = siteBiz;
            //var r = serviceProvider.GetRequiredService<ILogger<Test>>();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
