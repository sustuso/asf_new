﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace SustApp.ASF.ConsoleApp
{
    class Program
    {
        static Task Main(string[] args)
        {
            return Host.CreateDefaultBuilder()
                .ConfigureServices((context, services) =>
                {
                    var connectionString = context.Configuration.GetConnectionString("db");
                    services.AddEFDataLayer(b => b.UseSqlServer(connectionString));
                    services.AddBiz();

                    services.AddTransient<Test>();

                    services.AddHostedService<Test>();
                })
                .RunConsoleAsync();
        }
    }
}
